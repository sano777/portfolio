/***************************************************************
 * Name:      GererBDDStation.h
 * Auteur:    Clément DEFAUX
 **************************************************************/
#ifndef GERERBDDSTATION_H
#define GERERBDDSTATION_H

#include <wx/string.h>
#include <sqlite3.h>
#include "ihm/EvtStationMeteo.h"
#include "ConfigurationStationMeteo.h"



class EvtStationMeteo;
class ConfigurationStationMeteo;
class GererBDDStation
{
    public:

        GererBDDStation(EvtStationMeteo*);
        virtual ~GererBDDStation();
        wxString LitBDDSynchro(wxString,wxString);
        wxString LitBDDSynchro(wxString);
        void EcritValeur(wxString,float);
        void SupprimeValeur();
        int Etat;
        sqlite3 *SQLite;


    protected:
    private:
        static wxString trame;
        static wxString DateMax;
        static int callback(void *NotUsed, int argc, char **argv, char **azColName);
        ConfigurationStationMeteo * ConfigStation;
        EvtStationMeteo * FrameStation;
        wxString VerifieDate(wxString);
};

#endif // GERERBDDSTATION_H
