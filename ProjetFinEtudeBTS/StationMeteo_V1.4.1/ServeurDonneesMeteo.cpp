#include "ServeurDonneesMeteo.h"


ServeurDonneesMeteo::ServeurDonneesMeteo(EvtStationMeteo *frame, GererBDDStation *ptrGererBDD, long port, int maxclients)
{
    Acces_GererBDD = ptrGererBDD;
    m_frame=frame;
    m_port=port;
    wxIPV4address addr;
    addr.Service(m_port);
    m_serveur = new wxSocketServer(addr);

    wxString message(wxT(""));
    // on vérifie que le serveur est bien à l'écoute
    if (! m_serveur->Ok())
    {
        m_serveur_running=false;
    }
    else
        // le serveur fonctionne
    {
        m_serveur_running=true;
        // on demande à l'IHM d'afficher le message
        message.sprintf(wxT("Serveur à l'écoute sur le port %i...\n"),m_port);
        wxCommandEvent MyEvent(wxEVT_COMMAND_BUTTON_CLICKED,ID_SERVEUR);
        MyEvent.SetString(message);
        wxPostEvent(m_frame, MyEvent);
        // On connecte les gestionnaires d'événement au événements
        Connect( SERVER_ID, wxEVT_SOCKET , wxSocketEventHandler( ServeurDonneesMeteo::OnServerEvent ));
        Connect( SOCKET_ID, wxEVT_SOCKET , wxSocketEventHandler( ServeurDonneesMeteo::OnSocketEvent ));
        // On indique quel gestionnaire d'événement utiliser
        m_serveur->SetEventHandler(*this, SERVER_ID);
        // sur un événement 'connexion au socket'
        m_serveur->SetNotify(wxSOCKET_CONNECTION_FLAG);
        // on active les événements sur le socket
        m_serveur->Notify(true);
        // au départ pas de client connecté
        m_nombreClients=0;
    }

 }

ServeurDonneesMeteo::~ServeurDonneesMeteo()
{
    //dtor
}

void ServeurDonneesMeteo::Close()
{
    m_serveur_running=false;
    m_serveur->Destroy();
}



// gestionnaire d'événement pour le serveur
void ServeurDonneesMeteo::OnServerEvent(wxSocketEvent& event)
{
    wxSocketBase *sock;

    // si l'événement est une nouvelle connexion sur le socket
    if (event.GetSocketEvent()==wxSOCKET_CONNECTION)
    {
        // alors on accepte cette nouvelle connexion en créant un
        // nouveau socket et ceci de façon non bloquante.
        // Ce n'est pas génant car si nous sommes arrivés ici
        // c'est qu'il y a de toute façon une connexion entrante
        sock = m_serveur->Accept(false);
        // si le socket existe
        if (sock && m_nombreClients==0)     //N'accepte qu'un seul client
        {
            // on indique quel gestionnaire d'événement utiliser (pour ce nouveau socket)
            sock->SetEventHandler(*this, SOCKET_ID);
            // sur un événement 'entrée sur le socket' ou 'socket perdu'
            sock->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
            // on active les événements sur ce socket
            sock->Notify(true);
            // il y a un client de plus
            m_nombreClients++;
            int num_client=1;
            wxString message(wxT("Connexion du client\n"));
            // on envoie son numéro au client
            sock->Write(&num_client,sizeof (int));
            AfficheMessage(message);
            AfficheNombreClients();
        }
        else
        {
            wxLogWarning(wxT("le serveur n'accepte plus de client"));
            return;
        }
    }
    else
    // à priori, il ne peut pas y avoir d'autre événement qu'une nouvelle connexion sur le socket
    {
        AfficheMessage(wxT("Événement inattendu sur OnServerEvent"));
    }
}

// gestionnaire d'événement pour un socket
void ServeurDonneesMeteo::OnSocketEvent(wxSocketEvent& event)
{
    // on récupère le socket ayant généré l'événement
    wxSocketBase *sock = event.GetSocket();

    // on traite l'événement sur ce socket
    switch(event.GetSocketEvent())
    {
        // entrée sur le socket
        case wxSOCKET_INPUT:
        {
            // On déactive l'événement d'entrée sur le socket
            // de façon à ne pas réentrer dans le gestionnaire d'événement
            // pendant le traitement.
            sock->SetNotify(wxSOCKET_LOST_FLAG);
            // on récupère les données pour ce client
            wxString message(wxT(""));
            message = LitMessage(sock);
            // on affiche le message sur l'ihm
            wxString s(wxT(""));
            s.Printf(wxT("message reçu: %s \n"),message.c_str());
            m_frame->m_textCtrlDebugServeur->AppendText(s);
            // on décode le message du client et on lui répond
            DecodeMessage(message,sock);
            // on rétablit l'événement d'entrée sur le socket
            sock->SetNotify(wxSOCKET_LOST_FLAG | wxSOCKET_INPUT_FLAG);
            break;
        }
        // le client s'est déconnecté
        case wxSOCKET_LOST:
        {
            m_nombreClients--;
            // on détruit le socket
            sock->Destroy();
            wxString message(wxT("Déconnexion du client \n"));
            AfficheMessage(message);
            AfficheNombreClients();
            break;
        }
        default:
            AfficheMessage(wxT("événement inattendu sur OnSocketEvent"));
            break;
    }
}

void ServeurDonneesMeteo::DecodeMessage (wxString message, wxSocketBase *sock)
{
    string reponse="";

    //Demande de données météo
    if( message.at(0)>='1' && message.at(0)<='8' && message.at(1)=='0' && message.at(2)=='0' )
    {
        wxString rep=wxT("");
        wxString date=wxT("");
        wxString Capteur=wxT("");
        int Cap = message.GetChar(0) - 48;

        Capteur=TrouveNomCapteur(Cap);
        date=message.AfterFirst('+');
        date=date.BeforeFirst('!');

        if(date.IsEmpty())
        {
            rep=Acces_GererBDD->LitBDDSynchro(Capteur);
        }
        else
        {
            rep=Acces_GererBDD->LitBDDSynchro(Capteur,date);
        }
        reponse = rep.mb_str(wxConvUTF8);
        m_frame->m_textCtrlDebugServeur->AppendText(wxT("Réponse : ") + rep +wxT("\n\n"));
    }
    else
    {
        m_frame->m_textCtrlDebugServeur->AppendText(wxT("Réponse : Demande incorrecte...\n\n"));
        reponse.assign("Demande incorrecte...");
    }

    EcritReponse(sock,reponse);
}

void ServeurDonneesMeteo::EcritReponse(wxSocketBase *sock, string reponse)
{
    string buffer = reponse;
    buffer.push_back(0x0D);
    buffer.push_back(0x0A);
    buffer.push_back(0x00);
    sock->Write( buffer.c_str() , strlen(buffer.c_str()) );
}

void ServeurDonneesMeteo::AfficheNombreClients()
{
    wxCommandEvent MyEvent(wxEVT_COMMAND_BUTTON_CLICKED,ID_SERVEUR);
    wxString message(wxT(""));
    message.Printf(wxT("%i client connecté\n"),m_nombreClients);
    MyEvent.SetString(message);
    wxPostEvent(m_frame, MyEvent);
}

void ServeurDonneesMeteo::AfficheMessage(wxString message)
{
    wxCommandEvent MyEvent(wxEVT_COMMAND_BUTTON_CLICKED,ID_SERVEUR+1);
    MyEvent.SetString(message);
    wxPostEvent(m_frame, MyEvent);
}


wxString ServeurDonneesMeteo::LitMessage(wxSocketBase *sock)
{
    // à mettre absolument de façon à éviter de rester bloqué dans le gestionnaire d'événement
    // et donc de réentrer dans le code.
    sock->SetFlags(wxSOCKET_NOWAIT);

    char buffer;
    wxString reponse;
    do
    {
        sock->Read(&buffer, 1);
        wxChar buffer2=buffer;
        reponse<<buffer2;
    } while (buffer != 0x0A); // jusqu'à le 'LF'
    //On enlève le CR LF
    reponse.RemoveLast();
    reponse.RemoveLast();
    return reponse;
}

wxString ServeurDonneesMeteo::TrouveNomCapteur(const int num)
{
    wxString capteur=wxT("");
    switch (num) // on indique sur quelle variable on travaille
    {
        case 1: capteur=wxT("inttemp"); break;
        case 2: capteur=wxT("inthygro"); break;
        case 3: capteur=wxT("intbaro"); break;
        case 4: capteur=wxT("exttemp"); break;
        case 5: capteur=wxT("exthygro"); break;
        case 6: capteur=wxT("extanemo"); break;
        case 7: capteur=wxT("extgirouette"); break;
        case 8: capteur=wxT("extpluvio"); break;
        default: break;
    }
    return capteur;
}
