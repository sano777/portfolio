/***************************************************************
 * Name:      ThreadDonneesMeteo.h
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

 /*************************************************************/
 #ifndef THREADDONNEESMETEO_H
 #define THREADDONNEESMETEO_H
/*************************************************************/

//////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <wx/log.h>
#include "ihm/EvtStationMeteo.h"
#include "ConfigurationStationMeteo.h"
/////////////////////////////////////////////////////////////

using namespace std;

class EvtStationMeteo;
class ConfigurationStationMeteo;
class GererBDDStation;

class ThreadDonneesMeteo: public wxThread
{
    public:
        void Findemander();
        virtual void *Entry();
        virtual void OnExit();
        virtual ~ThreadDonneesMeteo();
        void DecodageManchester(char* TrameC);
        void SetFinDemande();
        ThreadDonneesMeteo(EvtStationMeteo *frame,ConfigurationStationMeteo *ptrConfig,GererBDDStation *ptrGererBDD);

    protected:

    private:
        int  fd;
        int  res;
        int  Etat;
        char buf[2];
        int  Compteur;
        bool fin_demandee;
        bool Configuration();
        bool EcrireValeurExt;
        bool EcrireValeurInt;
        unsigned char Byte[11];
        bool EcrireValeurPluie;
        EvtStationMeteo *MonIHM;
        vector<bool> TrameBinaire;
        struct termios oldtio,newtio;
        GererBDDStation * Acces_GererBDD;
        vector<bool> DecodageManchesterTrame;
        ConfigurationStationMeteo * RecupDonnees;
        void ProtocoleCapteurDeRGR918(unsigned char *Byte);
        void ProtocoleCapteurDeWGR918(unsigned char *Byte);
        void ProtocoleCapteurDeTHGR918N(unsigned char *Byte);
        void ProtocoleCapteurDeTHGR918EXT(unsigned char *Byte);
        void ProtocoleOregon(vector<bool> DecodageManchester);

};

#endif // THREADDONNEESMETEO_H
