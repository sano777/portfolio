/***************************************************************
 * Name:      ThreadDonneesMeteo.cpp
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

///////////////////////////////////////////////////////////////
#include "ThreadDonneesMeteo.h"
//////////////////////////////////////////////////////////////

ThreadDonneesMeteo::ThreadDonneesMeteo(EvtStationMeteo *frame,ConfigurationStationMeteo *ptrConfig,GererBDDStation *ptrGererBDD):wxThread(wxTHREAD_JOINABLE)
{
    Etat = 1;
    Compteur = 0;
    MonIHM = frame;
    EcrireValeurInt = true;
    EcrireValeurExt = true;
    EcrireValeurPluie = true;
    RecupDonnees = ptrConfig;
    Acces_GererBDD = ptrGererBDD;
    //Sinon n'est pas connecté a la base de données on éteint le thread
    if(!Acces_GererBDD->Etat){
        MonIHM->SetFinDemandee();
    }
}

ThreadDonneesMeteo::~ThreadDonneesMeteo()
{
    //On coupe la liaison série.
   close(fd);
}

void *ThreadDonneesMeteo::Entry()
{
    //Si la Configuration a réussie, on allume la LED pour signaler que le thread est en lecture
    if(this->Configuration()){
      MonIHM->LedPortSerieOff(wxT("Lecture du port."));
        try{
            //Tant qu'on ne demande pas la fin, on lit le port série
            while(!MonIHM->GetFinDemandee()){
             //On récupére un caractère du port série
             res = read(fd,buf,1);
             buf[res]=0;
             if(res != 0){
                //On passe le buffer à la méthode DecodageManchester()
                DecodageManchester(buf);
             }
             else{
                 //Sinon on eteint la led, on affiche le message et on coupe le thread
                MonIHM->LedPortSerieOff(wxT("Sortie sur un time out."));
                MonIHM->SetFinDemandee();
             }
          }
       }
       catch(exception& e){
            wxString Error(e.what(), wxConvUTF8);
       }
    }
    else{
        //Sinon on eteint la LED, on affiche le message et on coupe le thread
     MonIHM->LedPortSerieOff(wxT("Pas de port série"));
     MonIHM->SetFinDemandee();
    }
}

void ThreadDonneesMeteo::OnExit()
{
//On coupe la liaison série.
  close(fd);
}

bool ThreadDonneesMeteo::Configuration()
{
    long ValeurVitesse;
    long BitDonnee;

    try{
        /*****************Affectation de l'adresse du port série******************/
        //On récupe l'adress du port.
        wxString AdressPort(RecupDonnees->GetAdressPort());
        //On ouvre le port avec l'adresse récupère
        fd = open(AdressPort.mb_str(), O_RDWR | O_NOCTTY );
        //Si le port n'exite pas, on retourne Zéro.
        if (fd < 0){
            return 0;
        }
        /************************************************************************/

        tcgetattr(fd,&oldtio);
        bzero(&newtio, sizeof(newtio));

        /*****************Affectation la vitesse au port série******************/
        //On récupère la valeur de la vitesse en on l'affecte
        wxString Vitesse = RecupDonnees->GetVitesse();
        Vitesse.ToLong(&ValeurVitesse);
        switch(ValeurVitesse){
            case 150:
                newtio.c_cflag = B150;
                break;
            case 300:
                newtio.c_cflag = B300;
                break;
            case 600:
                newtio.c_cflag = B600;
                break;
            case 1200:
                newtio.c_cflag = B1200;
                break;
            case 2400:
                newtio.c_cflag = B2400;
                break;
            case 4800:
                newtio.c_cflag = B4800;
                break;
            case 9600:
                newtio.c_cflag = B9600;
                break;
            case 19200:
                newtio.c_cflag = B19200;
                break;
            case 38400:
                newtio.c_cflag = B38400;
                break;
            case 57600:
                newtio.c_cflag = B57600;
                break;
            case 115200:
                newtio.c_cflag = B115200;
                break;
            case 230400:
                newtio.c_cflag = B230400;
                break;
            case 460800:
                newtio.c_cflag = B460800;
                break;
            case 921600:
                newtio.c_cflag = B921600;
                break;
            }//fin du switch
        /******************************************************************************/

        newtio.c_cflag |= CRTSCTS;

        /****************Affectation de la parité au port série***********************/
        //On recupére l'option de la parité et on l'effect
        wxString Partie = RecupDonnees->GetParite();
        if(Partie == wxT("NONE")){
            newtio.c_cflag &= ~(PARENB|CSIZE);
        }
        else if(Partie == wxT("ODD")){
            newtio.c_cflag |= PARODD;
        }
        else if(Partie == wxT("ENB")){
            newtio.c_iflag |= PARENB;
        }
        else{
            newtio.c_cflag &= ~(PARENB|CSIZE);
        }
        /*****************************************************************************/

        /***********Affectation du nombre de byte au port série***********************/
        //On récupère le on de bit est on l'affecte
        wxString BitD = RecupDonnees->GetBitDonnees();
        BitD.ToLong(&BitDonnee);
        switch(BitDonnee){
            case 5:
                newtio.c_cflag |= CS5;
                break;
            case 6:
                newtio.c_cflag |= CS6;
                break;
            case 7:
                newtio.c_cflag |= CS7;
                break;
            case 8:
                newtio.c_cflag |= CS8;
                break;
        }//fin du switch
        /****************************************************************************/
        newtio.c_cflag |= (CLOCAL | CREAD);

        newtio.c_oflag = 0;
        newtio.c_lflag = 0;
        newtio.c_cc[VTIME]     = 500; /*On attent 500ms avant un time-out*/
        newtio.c_cc[VMIN]      = 1; /*On met un caractère bloquant*/
        tcflush(fd, TCIFLUSH);
        tcsetattr(fd,TCSANOW,&newtio);
        }//fin du try
    catch(exception& e){
        wxString Error(e.what(), wxConvUTF8);
    }//fin du catch
    return 1;;
}

void ThreadDonneesMeteo::DecodageManchester(char *Trame)
{
 char TrameC = Trame[0];

  switch (Etat){
	case 1:
		Compteur=0;
		//on vide la trame decodage manchester
		DecodageManchesterTrame.clear();
		if( TrameC == 'm' ){
			Etat = 2;
		}
		break;
	case 2:

		if(TrameC == 'l'){
			Etat = 3;
			Compteur++;
		}
		else{
			Etat = 1;
		}
		break;
	case 3:
		if(Compteur<16 && TrameC == 'm'){
			Etat = 2;
		}
		else if(Compteur == 16 && TrameC == 'm'){
			Etat = 4;
		}
		else{
			Etat = 1;
		}
		break;
	case 4:
		if(TrameC == 'c'){
			Etat = 5;
		}
		else{
			Etat = 1;
		}
		break;
	case 5:
		if(TrameC == 'd'){
			Etat = 6;
		}
		else if(TrameC== 'm' || TrameC =='f'){
		    //On passe la trame au protole Orégon
			ProtocoleOregon(DecodageManchesterTrame);
		}
		else{
            Etat = 1;
		}
		break;
	case 6:
            //On ajoute à la fin de la trame un Zéro
			DecodageManchesterTrame.push_back(0);

		if(TrameC == 'l'){
			Etat = 10;
		}
		else if (TrameC == 'c'){
			Etat = 5;
		}
		else if(TrameC == 'f'){
		    //On passe la trame au protole Orégon
			ProtocoleOregon(DecodageManchesterTrame);
		}
		else{
            Etat = 1 ;
		}
		break;
	case 7:
		if(TrameC == 'c'){
			Etat = 8;
		}
		else if (TrameC == 'l' /*|| TrameC == 'm' || TrameC == 'f'*/){
		    //On passe la trame au protole Orégon
			ProtocoleOregon(DecodageManchesterTrame);
		}
		else{
			Etat = 1;
		}
		break;
	case 8:
            //On ajoute à la fin de la trame un Un
			DecodageManchesterTrame.push_back(1);
        if(TrameC == 'm'){
			Etat = 9;
		}
		else if(TrameC == 'f'){
		    //On passe la trame au protole Orégon
			 ProtocoleOregon(DecodageManchesterTrame);
		}
		else{
            Etat = 1;
		}
		break;
	case 9:
            //On ajoute à la fin de la trame un Zéro
			DecodageManchesterTrame.push_back(0);
		if(TrameC == 'l'){
			Etat = 10;
		}
		else if(TrameC == 'c'){
			Etat = 5;
		}
		else if (TrameC == 'f'/*|| TrameC == 'm'*/){
		    //On passe la trame au protole Orégon
			ProtocoleOregon(DecodageManchesterTrame);
		}
		else{
            Etat = 1;
		}
		break;
	case 10:
            //On ajoute à la fin de la trame un Un
			DecodageManchesterTrame.push_back(1);
		if(TrameC == 'f'){
		    //On passe la trame au protole Orégon
			ProtocoleOregon(DecodageManchesterTrame);
		}
		else if (TrameC == 'd'){
			Etat =7;
		}
		else if (TrameC == 'm'){
			Etat =9;
		}
		else{
           Etat = 1;
		}
		break;
    }//Fin du switch
}//FIn de decodage trame

void ThreadDonneesMeteo::ProtocoleOregon(vector<bool> DecodageManchester)
{
    Etat = 1;
    int AvanceOctet = 12;
	vector<bool>InfoMeteo;
	vector<bool>InfoAnemo;

    /******************************On prend 1 bit sur 2 de la trame reçus*********************************************/
    for(unsigned int i=0; i < DecodageManchester.size(); i++){
        if((i%2) == 0){
            InfoMeteo.push_back(DecodageManchester[i]);
        }
    }
    /*****************************************************************************************************************/

    //On vide le vector.
    DecodageManchesterTrame.clear();

    /********************************On divise par 2 la trame, Si elle est égale a 186*******************************/
    if(InfoMeteo.size() == 186){
        for(int i=0; i < 98; i++){
            InfoMeteo.pop_back();
        }
    }
    /***************************************************************************************************************/

    /*****************************************On détermine le nombre d'octet***************************************/
    switch(InfoMeteo.size()){
        case 80:
            AvanceOctet = 10;
        break;
        case 88:
            AvanceOctet = 11;
        break;
        case 84:
            AvanceOctet = 10;
        break;
        case 96:
            AvanceOctet = 12;
        break;
        default:
            AvanceOctet = 12;
        break;
    }
    /*************************************************************************************************************/

    /***************************Mise en forme des informations selon le procole orégon***************************/
    if(InfoMeteo.size() == 80 || InfoMeteo.size() == 88 || InfoMeteo.size() == 84 || InfoMeteo.size() == 96 )
    {
        for(int i = (InfoMeteo.size()); i > 0;  i--){
            if( (InfoMeteo[i]) == 0){
                //Masquage ET 0XFE
                Byte[AvanceOctet] =  Byte[AvanceOctet] & 0xFE ;
            }//fin if
            else{
                //Masquage OU 0x01
                Byte[AvanceOctet] =  Byte[AvanceOctet] | 0x01;
            }//fin else
            if( i%8 == 0 ){
                --AvanceOctet;
            }//fin if
            else{
                //Décalage d'un bite vers la gauche
                Byte[AvanceOctet] = Byte[AvanceOctet] << 0x01;
            }
        }
    /*************************************************************************************************************/

    /**********************Mis en forme de l'adresse du capteur*************************/
    //On affecte la 1er partie de l'adresse.
    int Adress = Byte[0];
    //On décale de 8 bits pour affecter la 2ème partie de l'adresse.
    Adress = Adress << 0x08;
    //On affecte la 2ème partie de l'adresse.
    Adress += Byte[1];
    /**********************************************************************************/

    /**************On envoye des données selon l'adresse du capteur*******************/
    switch(Adress)
    {
        //Capteur Oregon-THGR918N Inside Temp-Hydro-Baro
        case 0x5A6D:
            if(EcrireValeurInt == true){
                ProtocoleCapteurDeTHGR918N(Byte);
            }
            else{
                EcrireValeurInt = true;
            }
        break;
        //Capteur Oregon-WGR918 Jauge de  pluie
        case 0x2A1D:
            if(EcrireValeurPluie == true){
                ProtocoleCapteurDeRGR918(Byte);
            }
            else{
              EcrireValeurPluie = true;
            }
        break;
        case 0x1A3D:
            if(EcrireValeurExt == true){
               ProtocoleCapteurDeTHGR918EXT(Byte);
            }
            else{
                EcrireValeurExt = true;
            }
        break;
            //Capteur Oregon-WGR918 Anemometre
        case 0x3A0D:
            ProtocoleCapteurDeWGR918(Byte);
        break;
        }
    /********************************************************************************/
    }

}

void ThreadDonneesMeteo::ProtocoleCapteurDeTHGR918N(unsigned char *Byte)
{
    wxString Barometre;
    int Humidite = 0;
    int Humidite2 = 0;
    int Baro = 856;
    int Temperature2 = 0;
    double Temperature = 0;
    double Temperature3 = 0;
    wxString HumiditeInt;
    wxString TemperatureInt;


    /****************************Mise en forme de la température en Degré Celsius*******************************/
    //Affectation des dizaines.
    Temperature = Byte[5]>>0x04;
    Temperature = Temperature * 10;

    //Affectation des unites.
    Byte[5] = Byte[5]<<0x04;
    Temperature2 = Byte[5]>>0x04;
    Temperature = Temperature + Temperature2;

    //Affectation des dixaines.
    Temperature3 = Byte[4]>>0x04;
    Temperature3 = Temperature3 * 0.1;
    Temperature = Temperature + Temperature3;

    //On affecte la température à un wxString pour l'affichage.
    TemperatureInt<<Temperature;
    //On sauvegarde la température dans la basse de donnnées.
    float TemperatureSauve = Temperature;
    Acces_GererBDD->EcritValeur(wxT("inttemp"),TemperatureSauve);
    /*******************************************************************************************************/

    /********************************Mise en forme de l'état de la batterie********************************/
    //On décale de 4 bits pour avoir que la valeur de la batterie.
    Byte[4] = Byte[4] << 0x04;
    //On décale de 4 bits pour récupère la valeur de la batterie et on l'affecte.
    int batterie =  Byte[4] >> 0x04;
    //On multiplie par 10 pour avoir l'état de la batterie.
    batterie = batterie * 10;
    //On retire à 100 la valeur de la batterie, pour avoir le pourcentage de la batterie pleine.
    batterie = 100 - batterie;
    /*****************************************************************************************************/

    /*****************************Mise en forme de la préssion athmosphérique****************************/
    //On ajoute 856 à la valeur que l'on a reçut.
    double ValeurBaro = Baro + Byte[8];
    //On affecte la préssion à un wxString pour l'affichage.
    Barometre << ValeurBaro;
    //On sauvegarde la préssion dans la basse de donnnées.
    float BaroSauve = ValeurBaro;
    Acces_GererBDD->EcritValeur(wxT("intbaro"),BaroSauve);
    /***************************************************************************************************/

    /****************************Mise en forme de l'humidité*******************************************/
    //Affectation des dizaines.
    Byte[7] = Byte[7]<<0x04;
    int Humidite1 = Byte[7]>>0x04;
    Humidite1 = Humidite1 * 10;

    //Affectation des unités.
    Humidite2 = Byte[6]>>0x04;
    Humidite = Humidite1 + Humidite2;

    //On affecte l'humidité à un wxString pour l'affichage.
    HumiditeInt<<Humidite;
    //On sauvegarde l'humidité dans la basse de donnnées.
    float HumiditeS = Humidite;
    Acces_GererBDD->EcritValeur(wxT("inthygro"),HumiditeS);//Humidité 2
    /***************************************************************************************************/

    /****************************Affichage des données dans l'IHM*******************************************/
    //Si UniteInt est égale à 1, on doit affiché la température en Fahrenheit.
    if(MonIHM->GetUniteInt()){
        //On vite la valeur qui est dans le string.
        TemperatureInt.clear();
        //On convertir la valeur en Fahrenheit.
        Temperature = (( 9 * Temperature ) / 5 ) + 32;
        //On affectue la nouvelle valeur.
        TemperatureInt<<Temperature;
    }
    //On prend le mutex de l'IHM et on affiche les valeurs de la température, humidité, barometre et de la batterie.
    wxMutexGuiEnter();
        MonIHM->WriteTemperatureInt(TemperatureInt);
        MonIHM->WriteHimiditeInt(HumiditeInt);
        MonIHM->WriteBarometre(Barometre);
        MonIHM->WriteBatterieInt(batterie);
        MonIHM->ImageDuCiel(ValeurBaro);
    //On rend le mutex de l'IHM.
    wxMutexGuiLeave();
    /******************************************************************************************************/

    //On met EcrireValeurInt à false pour éviter une 2ème écriture.
    EcrireValeurInt = false;
}

void ThreadDonneesMeteo::ProtocoleCapteurDeRGR918(unsigned char *Byte)
{
    double Rainfall= 0;
    wxString Rain;

    /**************************Mise en forme de la donnée pluie qui est tombée******************************/
    //Affectation des centaines
    Rainfall = Byte[5] >> 0x04;
    Rainfall = Rainfall *100;

    //Affectation des dizaines
    Byte[5] = Byte[5] << 0x04;
    int Rainfall2 = Byte[5] >> 0x04;
    Rainfall2 = Rainfall2 * 10;
    Rainfall = Rainfall + Rainfall2;

    //Affectation des unités
    double Rainfall3 = Byte[4] >> 0x04;
    Rainfall  = Rainfall + Rainfall3;

    //On affecte la pluie à un wxString pour l'affichage.
    Rain << Rainfall;
    //On sauvegarde la pluie dans la basse de donnnées.
    float RainSauve = Rainfall;
    Acces_GererBDD->EcritValeur(wxT("extpluvio"),RainSauve);
    /*******************************************************************************************************/

    /********************************Mise en forme de l'état de la batterie********************************/
    //On décale de 4 bits pour avoir que la valeur de la batterie.
    Byte[4] = Byte[4] << 0x04;
    //On décale de 4 bits pour récupère la valeur de la batterie et on l'affecte.
    int batterie =  Byte[4] >> 0x04;
    //On multiplie par 10 pour avoir l'état de la batterie.
    batterie *= 10;
    //On retire à 100 la valeur de la batterie, pour avoir le pourcentage de la batterie pleine.
    batterie = 100 - batterie;
    /*******************************************************************************************************/

    /****************************Affichage des données dans l'IHM*******************************************/
    //On prend le mutex de l'IHM et on affiche les valeurs de la pluie et de la batterie.
    wxMutexGuiEnter();
        MonIHM->WritePuie(Rain);
        MonIHM->WriteBatteriePluie(batterie);
    //On rend le mutex de l'IHM.
    wxMutexGuiLeave();
    /******************************************************************************************************/

    //On met EcrireValeurPluie à false pour éviter une 2ème écriture.
    EcrireValeurPluie = false;
    //On met EcrireValeurInt à true pour que le module intérieur puisse écrire.
    EcrireValeurInt = true;
}

void ThreadDonneesMeteo::ProtocoleCapteurDeWGR918(unsigned char *Byte)
{
    int Direction = 0;
    double Vitesse = 0;
    wxString VitesseDuVent;
    wxString DirectionDuVent;

    /********************Mis en forme de la donnée Direction du vent***************************************/
    //Affectation des centaines
    Direction = Byte[5] >> 0x04;
    Direction = Direction * 100;

    //Affectation des dizaines
    Byte[5] = Byte[5] << 0x04;
    int Direction2 = Byte[5] >> 0x04;
    Direction2 = Direction2 *10;

    //Affectation des unités
    int Direction3 = Byte[4] >> 0x04;
    Direction = Direction + Direction2 + Direction3;

    //On affecte la Direction à un wxString pour l'affichage.
    DirectionDuVent<<Direction;
    //On sauvegarde la Direction dans la basse de donnnées.
    float DirectionSauv = Direction;
    Acces_GererBDD->EcritValeur(wxT("extgirouette"),DirectionSauv);
    /******************************************************************************************************/

    /***************************Mis en forme de la donnée vitesse du vent**********************************/
    //Affectation des dizaines
    Byte[7] = Byte[7] << 0x04;
    Vitesse = Byte[7] >> 0x04;
    Vitesse *= 10;

    //Affectation des unités
    double Vitesse2 = Byte[6] >> 0x04;
    Vitesse = Vitesse + Vitesse2;

    //Affectation des dizièmes
    Byte[6] = Byte[6] << 0x04;
    double Vitesse3 = Byte[6] >> 0x04;
    Vitesse3 = Vitesse3 * 0.1;
    Vitesse = Vitesse + Vitesse3;

    //On affecte la Vitesse à un wxString pour l'affichage.
    VitesseDuVent<<Vitesse;
    //On sauvegarde la Vitesse dans la basse de donnnées.
    float VitesseSauv = Vitesse;
    Acces_GererBDD->EcritValeur(wxT("extanemo"),VitesseSauv);
    /******************************************************************************************************/

    /********************************Mise en forme de l'état de la batterie********************************/
    //On décale de 4 bits pour avoir que la valeur de la batterie.
    Byte[4] = Byte[4] << 0x04;
    //On décale de 4 bits pour récupère la valeur de la batterie et on l'affecte.
    int batterie =  Byte[4] >> 0x04;
    //On multiplie par 10 pour avoir l'état de la batterie.
    batterie = batterie * 10;
    //On retire à 100 la valeur de la batterie, pour avoir le pourcentage de la batterie pleine.
    batterie = 100 - batterie;
    /******************************************************************************************************/

    /****************************Affichage des données dans l'IHM******************************************/
    //On prend le mutex de l'IHM et on affiche les valeurs de la pluie et de la batterie.
    wxMutexGuiEnter();
        MonIHM->WriteDirectionVent(DirectionDuVent);
        MonIHM->DirectionDuVent(Direction);
        MonIHM->WriteVitesseVent(VitesseDuVent);
        MonIHM->WriteBatterieGirouette(batterie);
    //On rend le mutex de l'IHM.
    wxMutexGuiLeave();
    /*****************************************************************************************************/

    //On met EcrireValeurInt à true pour que le module intérieur puisse écrire.
    EcrireValeurInt = true;
}

void ThreadDonneesMeteo::ProtocoleCapteurDeTHGR918EXT(unsigned char *Byte)
{
    double Hum = 0;
    double Temp = 0;
    int batterie = 0;
    wxString HumExt;
    wxString TempExt;

    /*******************************Mise en forme de la température en C°************************************/
    //Affectation des dizaines
    Temp = Byte[5] >> 0x04;
    Temp = Temp *10;

    //Affectation des unités
    Byte[5] = Byte[5] << 0x04;
    int temp2 = Byte[5] >> 0x04;
    Temp = Temp + temp2;

    //Affectation des dizièmes
    double temp3 = Byte[4]>>0x04;
    temp3 = temp3 * 0.1;
    Temp = Temp + temp3;

    //Affichage de la température et enregistré la valeur dans la base de données
    TempExt<<Temp;
    float TempSauve = Temp;
    Acces_GererBDD->EcritValeur(wxT("exttemp"),TempSauve);
    /********************************************************************************************************/

    /********************************Mise en forme de l'humidité*********************************************/
    //Affectation des dizaines
    Byte[7] = Byte[7] << 0x04;
    int hum1= Byte[7] >> 0x04;
    hum1 = hum1 * 10;

    //Affectation des unités
    int hum2= Byte[6] >> 0x04;
    Hum = hum1 + hum2;

    //Affichage de l'humidité et enregistré la valeur dans la base de données
    HumExt<<Hum;
    float HumSauve = Hum;
    Acces_GererBDD->EcritValeur(wxT("exthygro"),HumSauve);
    /********************************************************************************************************/

    /********************************Mise en forme de l'état de la batterie*********************************/
    //On décale de 4 bits pour avoir que la valeur de la batterie.
    Byte[4] = Byte[4] << 0x04;
     //On décale de 4 bits pour récupère la valeur de la batterie et on l'affecte.
    batterie =  Byte[4] >> 0x04;
    //On multiplie par 10 pour avoir l'état de la batterie.
    batterie = batterie * 10;
    //On retire à 100 la valeur de la batterie, pour avoir le pourcentage de la batterie pleine.
    batterie = 100 - batterie;
    /******************************************************************************************************/


    /****************************Affichage des données dans l'IHM******************************************/
    //Si UniteInt est égale à 1, on doit affiché la température en Fahrenheit.
    if(MonIHM->GetUniteExt()){
        //On vite la valeur qui est dans le string.
        TempExt.clear();
        //On convertir la valeur en Fahrenheit.
        Temp = (( 9 * Temp ) / 5 ) + 32;
        //On affectue la nouvelle valeur.
        TempExt<<Temp;
    }
    //On prend le mutex de l'IHM et on affiche les valeurs de la pluie et de la batterie.
    wxMutexGuiEnter();
        MonIHM->WriteHumiditeExt(HumExt);
        MonIHM->WriteTemperatureExt(TempExt);
        MonIHM->WriteBatterieTempHum(batterie);
    //On rend le mutex de l'IHM.
    wxMutexGuiLeave();
    /*****************************************************************************************************/

     //On met EcrireValeurExt à false pour éviter une 2ème écriture.
    EcrireValeurExt = false;
    //On met EcrireValeurInt à true pour que le module intérieur puisse écrire.
    EcrireValeurInt = true;
}

