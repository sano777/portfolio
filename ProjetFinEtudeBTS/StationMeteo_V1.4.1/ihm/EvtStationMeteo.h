/***************************************************************
 * Name:      EvtStationMeteo.h
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

/*************************************************************/
 #ifndef __EvtStationMeteo__
 #define __EvtStationMeteo__
/*************************************************************/
///////////////////////////////////////////////////////////////
 #include <wx/log.h>
 #include "StationMeteo.h"
 #include "../ConfigurationStationMeteo.h"
 #include "../ThreadDonneesMeteo.h"
 #include "../GererBDDStation.h"
 #define ID_THREAD_DONNEEMETEO 200
 #define ID_SERVEUR 200
///////////////////////////////////////////////////////////////

class GererBDDStation;
class ThreadDonneesMeteo;
class ServeurDonneesMeteo;

class EvtStationMeteo : public StationMeteo
{
private:
    bool UniteInt;
    bool UniteExt;
    wxTimer m_timer;
    bool FinDemandee;
    GererBDDStation *GererBDD;
    void OnTimer(wxTimerEvent&);
    ServeurDonneesMeteo *m_serveur;
    ConfigurationStationMeteo * config;
    void OnFrameClose( wxCloseEvent& event );
    ThreadDonneesMeteo * thread_donneesmeteo;
    void AfficheMessageServeur (wxCommandEvent& event);

protected:
	// Handlers for StationMeteo events.
	void OnClickButtonArret( wxCommandEvent& event );
	void GetValueExtTemp( wxMouseEvent& event );
	void GetValueIntTemp( wxMouseEvent& event );
	void OnClickValider( wxCommandEvent& event );
	void OnClickDefaut( wxCommandEvent& event );
	void OnButtonServeur( wxCommandEvent& event );
	//void OnThreadDonneeMeteo(wxCommandEvent & event);
	void OnClickButtonQuitter( wxCommandEvent& event );
	void OnClickButtonRelancer( wxCommandEvent& event );


public:
    void LedPortSerieOn();
    void DirectionDuVent(int angle);
	void ImageDuCiel(double Barometre);
	EvtStationMeteo( wxWindow* parent ); /** Constructor **/
	bool GetUniteInt(){return UniteInt;}
	bool GetUniteExt(){return UniteExt;}
	void LedPortSerieOff(const wxString Etat);
	void SetFinDemandee(){FinDemandee = true;}
	bool GetFinDemandee(){return FinDemandee;}
	void WritePuie(wxString Pluie){m_staticTextPluie->SetLabel(Pluie);}
	void WriteBatterieInt(int EtatBatterie){m_gaugeInt->SetValue(EtatBatterie);}
	void WriteBarometre(wxString Barometre){m_staticTextBaro->SetLabel(Barometre);}
	void WriteVitesseVent(wxString Vitesse){m_staticTextVitesse->SetLabel(Vitesse);}
	void WriteHumiditeExt(wxString HumiditeExt){m_staticHumExt->SetLabel(HumiditeExt);}
	void WriteHimiditeInt(wxString HumiditeInt){ m_staticHumInt->SetLabel(HumiditeInt);}
	void WriteDirectionVent(wxString Direction){m_staticTextDirect->SetLabel(Direction);}
	void WriteBatteriePluie(int EtatBatteriePluie){m_gaugePluie->SetValue(EtatBatteriePluie);}
	void WriteTemperatureExt(wxString TemperatureExt){m_staticTempExt->SetLabel(TemperatureExt);}
	void WriteTemperatureInt(wxString TemperatureInt){m_staticTempInt->SetLabel(TemperatureInt);}
	void WriteBatterieTempHum(int EtatBatterieTempHum){m_gaugeTempHum->SetValue(EtatBatterieTempHum);}
	void WriteBatterieGirouette(int EtatBatterieGirouette){m_gaugeGirouette->SetValue(EtatBatterieGirouette);}
};

#endif // __EvtStationMeteo__
