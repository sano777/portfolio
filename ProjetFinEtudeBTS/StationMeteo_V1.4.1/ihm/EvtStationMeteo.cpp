/***************************************************************
 * Name:      EvtStationMeteo.cpp
 * Author:    Sébastien Marie, Clément Defaux
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

///////////////////////////////////////////////////////////////
#include "EvtStationMeteo.h"
#include "../ServeurDonneesMeteo.h"
/////////////////////////////////////////////////////////////

EvtStationMeteo::EvtStationMeteo( wxWindow* parent )
:
StationMeteo( parent ),m_timer(this)
{
    UniteInt = 0;
    UniteExt = 0;
    FinDemandee = false;

    //Création des objets GererBDD et Configuration.
    GererBDD = new GererBDDStation(this);
    config = new ConfigurationStationMeteo();

    //On récupére les données de configuration dans le fichier XML et on l'affiche dans l'IHM.
    config->RecupereConfiguration();

    //Affichage de l'adresse du port série.
    m_textCtrlAdress->SetValue(config->GetAdressPort());
    //Affichage de la vitesse.
    m_comboBoxVitesse->SetValue(config->GetVitesse());
    //Affichage du nombre de bit par données.
    m_comboBoxDb->SetValue(config->GetBitDonnees());
    //Affichage de la valeur de parité.
    m_comboBoxPrt->SetValue(config->GetParite());
    //Affichage du numéro du port du serveur.
    m_spinCtrlPort->SetValue(config->GetPort());

    //Si on est connecté à la base de données on affiche connecté.
    if(GererBDD->Etat){
        m_staticTextCnnect->SetLabel(wxT("Connecté"));
    }
    //Sinon on affiche Non Connecté.
    else{
        m_staticTextCnnect->SetLabel(wxT("Non Connecté"));
    }

    //Création du thread.
    thread_donneesmeteo = new ThreadDonneesMeteo(this,config,GererBDD);
    //Si le thread n'a pas pus être créer on avertie l'utilisateur.
    if ( thread_donneesmeteo->Create() != wxTHREAD_NO_ERROR ){
        wxLogError(wxT("Création thread impossible!"));
    }
    //Sinon on met le thread en route
    else{
        thread_donneesmeteo->Run();
        //m_textCtrldebug->AppendText(wxT("Thread créée\n"));
    }

    //Placer ici le timer qui va appeler à intervales de temps régulier
    //les méthodes pour faire la synchro en réseau
    Connect(wxID_ANY,wxEVT_TIMER,wxTimerEventHandler(EvtStationMeteo::OnTimer),NULL,this);    // on connecte l'événement timer
    m_timer.Start(100,wxTIMER_ONE_SHOT);   //Lance le timer une unique fois au bout d'une seconde (démarrage)

}

//Sur évènement du timer supression des données plus vieille d'une semaine
void EvtStationMeteo::OnTimer(wxTimerEvent&)
{
    //On (re)définie le timer au temps voulu...
    m_timer.Start(3600000);

    GererBDD->SupprimeValeur();
    //GererBDD->LitBDDSynchro(1);
}

void EvtStationMeteo::GetValueExtTemp( wxMouseEvent& event )
{
  //Si l'utilisateur clic sur la température extérieur, on vas convertir les Degré Celsius en Fahrenheit ou Fahrenheit en Degré Celsius.
    double ValeurConv;
    wxString Valeur=wxT("");
    wxString NewValeur=wxT("");

    //On récupère la valeur qui a dans l'IHM.
    Valeur = m_staticTempExt->GetLabel();
    //Si la convertion de wxString en long ce passe bien on convertie.
    if(Valeur.ToDouble(&ValeurConv)){
        //Si UniteExt est égale à Zéro ça veut dire que l'on est en Degré Celsius.
        if(UniteExt == 0){
            //Convertion des Degré Celsius en Fahrenheit.
         ValeurConv  = (( 9 * ValeurConv ) / 5 ) + 32;
         NewValeur << ValeurConv;
            //On affiche la nouvelle valeur et on change l'unité.
         m_staticTempExt->SetLabel(NewValeur);
         m_staticTextExtUnit->SetLabel(wxT("°F"));
            //On met UniteExt à 1 pour dire que l'on est en Fahrenheit.
         UniteExt =1;
        }
        //Si UniteExt est égale à 1 ça veut dire que l'on est en Fahrenheit.
        else{
             //Convertion des Fahrenheit en Degré Celsius.
         ValeurConv = (( ValeurConv - 32 ) * 5 ) / 9;
         NewValeur << ValeurConv;
            //On affiche la nouvelle valeur et on change l'unité.
         m_staticTempExt->SetLabel(NewValeur);
         m_staticTextExtUnit->SetLabel(wxT("°C"));
           //On met UniteExt à 1 pour dire que l'on est en Degré Celsius.
         UniteExt = 0;
        }
    }
    //Si la convertion ne c'est pas bien passé on avertie l'utilisateur.
    else{
     wxLogMessage(wxT("Erreur de convertion"));
    }
}

void EvtStationMeteo::GetValueIntTemp( wxMouseEvent& event )
{
    //Si l'utilisateur clic sur la température Intérieur, on vas convertir les Degré Celsius en Fahrenheit ou Fahrenheit en Degré Celsius.
	double ValeurConv;
    wxString Valeur=wxT("");
    wxString NewValeur=wxT("");

    //On récupère la valeur qui a dans l'IHM.
    Valeur = m_staticTempInt->GetLabel();
    //Si la convertion de wxString en long ce passe bien on convertie.
    if(Valeur.ToDouble(&ValeurConv)){
        //Si UniteInt est égale à Zéro ça veut dire que l'on est en Degré Celsius.
        if(UniteInt == 0){
            //Convertion des Degré Celsius en Fahrenheit.
         ValeurConv  = (( 9 * ValeurConv ) / 5 ) + 32;
         NewValeur << ValeurConv;
           //On affiche la nouvelle valeur et on change l'unité.
         m_staticTempInt->SetLabel(NewValeur);
         m_staticTextIntUnit->SetLabel(wxT("°F"));
          //On met UniteInt à 1 pour dire que l'on est en Fahrenheit.
         UniteInt =1;
        }
        else{
            //Convertion des Fahrenheit en Degré Celsius.
         ValeurConv = (( ValeurConv - 32 ) * 5 ) / 9;
         NewValeur << ValeurConv;
           //On affiche la nouvelle valeur et on change l'unité.
         m_staticTempInt->SetLabel(NewValeur);
         m_staticTextIntUnit->SetLabel(wxT("°C"));
           //On met UniteInt à 1 pour dire que l'on est en Degré Celsius.
         UniteInt = 0;
        }
    }
    //Si la convertion ne c'est pas bien passé on avertie l'utilisateur.
    else{
     wxLogMessage(wxT("Erreur de convertion"));
    }
}

void EvtStationMeteo::OnClickValider( wxCommandEvent& event )
{
 //L'utilisateur valide la nouvelle configuration entrée
   wxString ValeurPort;

	 //On récupère l'adresse du port série.
   config->SetAdressPort(m_textCtrlAdress->GetValue());
    //On récupère la vitesse.
   config->SetVitesse(m_comboBoxVitesse->GetValue());
    //On récupère les bits de données.
   config->SetBitDonnees(m_comboBoxDb->GetValue());
    //On récupère la parité.
   config->SetParite(m_comboBoxPrt->GetValue());
    //On récupère le numero du port
   ValeurPort.Printf(wxT("%i"),m_spinCtrlPort->GetValue());
   config->SetPort(ValeurPort);

    //On sauvegarde les modifications dans le fichier XML.
   config->ModifierConfiguration();
}

void EvtStationMeteo::OnClickDefaut( wxCommandEvent& event )
{
	m_spinCtrlPort->SetValue(30000);
	m_textCtrlAdress->SetValue(wxT("/dev/ttyUSB0"));
	m_comboBoxVitesse->SetValue(wxT("57600"));
	m_comboBoxDb->SetValue(wxT("8"));
	m_comboBoxPrt->SetValue(wxT("NONE"));
}

void EvtStationMeteo::OnFrameClose( wxCloseEvent& event )
{
 //Si le serveur est allumé on l'éteint.
   if (m_serveur != NULL){
        m_serveur->Close();
        delete m_serveur;
    }
 //Si le thread tourne on l'arrête.
    if(!FinDemandee){
        FinDemandee = true;
    }
//Et on detruit l'IHM
    Destroy();
}

void EvtStationMeteo::OnButtonServeur( wxCommandEvent& event )
{
    long port = m_spinCtrlPort->GetValue();
    // on convertit les wxString
    if (port)
    {
        if ((port>1024) && (port<=65535))
        {
            m_serveur=new ServeurDonneesMeteo(this,GererBDD,port);
            if (m_serveur->IsOK())
            // le serveur a démarré
            {
               // on connecte les événements venant du serveur
                Connect( ID_SERVEUR, wxEVT_COMMAND_BUTTON_CLICKED ,
                        wxCommandEventHandler( EvtStationMeteo::AfficheMessageServeur ), NULL, this );
                //Connect( ID_SERVEUR+1, wxEVT_COMMAND_BUTTON_CLICKED ,
                        //wxCommandEventHandler( EvtStationMeteo::AfficheMessageServeur ), NULL, this );
                // on cache les objets servant à la connexion
                m_buttonDemarrerServeur->Hide();
                //Fit();
                Layout();
            }
            else
            // le serveur n'a pas pu démarrer
            {
                wxString message(wxT(""));
                message.sprintf(wxT("ERREUR ! : Le serveur n'a pas pu démarrer sur le port %i\n"),port);
                m_textCtrlLog->AppendText(message);
                delete m_serveur;
            }

        }
        else
        {
            m_textCtrlLog->AppendText(wxT("le numéro de port doit être compris entre 1025 et 65535 !\n"));
        }
    }
    else
    {
            m_textCtrlLog->AppendText(wxT("le numéro de port doit être un entier !\n"));
    }
}

void EvtStationMeteo::AfficheMessageServeur (wxCommandEvent& event)
{   // affiche dans le log sur événement du serveur
    m_textCtrlLog->AppendText(event.GetString());
}

void EvtStationMeteo::OnClickButtonQuitter( wxCommandEvent& event )
{
 //Si le thread tourne, on l'éteint.
   if(!FinDemandee){
        FinDemandee = true;
        LedPortSerieOff(wxT("Lecture arretée"));
   }
 //Sinon on avertie l'utilisateur que le thread est déjà éteint.
   else{
        wxLogWarning(wxT("La liaison série est déjà coupée"));
   }
}

void EvtStationMeteo::OnClickButtonRelancer( wxCommandEvent& event )
{ //Si le thread tourne, on le coupe.
    if(!FinDemandee){
        FinDemandee = true;
        //On crée un nouveau thread.
        thread_donneesmeteo = new ThreadDonneesMeteo(this,config,GererBDD);
        //Si le thread n'a pas pus être créer on avertie l'utilisateur.
        if ( thread_donneesmeteo->Create() != wxTHREAD_NO_ERROR ){
            wxLogError(wxT("Création thread impossible!"));
        }
        //Sinon on met le thread en route
        else{
            FinDemandee =false;
            thread_donneesmeteo->Run();
            m_textCtrldebug->AppendText(wxT("\nThread créée\n"));
        }
    }
    //Sinon on crée un nouveau thread
    else{
        thread_donneesmeteo = new ThreadDonneesMeteo(this,config,GererBDD);
        //Si le thread n'a pas pus être créer on avertie l'utilisateur.
        if ( thread_donneesmeteo->Create() != wxTHREAD_NO_ERROR ){
            wxLogError(wxT("Création thread impossible!"));
        }
        //Sinon on met le thread en route
        else{
            FinDemandee = false;
            thread_donneesmeteo->Run();
            m_textCtrldebug->AppendText(wxT("Thread créée\n"));
        }
    }
}

void EvtStationMeteo::ImageDuCiel(double Barometre)
{
    //Si la valeur du barometre en compris entre 940 et 990, alors on affiche l'image pluie.bmp.
    if(Barometre >940 && Barometre < 990){
        m_bitmap1->SetBitmap(wxBitmap(wxImage(_T("image/pluie.bmp")).Rescale(wxSize(95,95).GetWidth(),wxSize(84,84).GetHeight())));
    }
    //Si la valeur du barometre en compris entre 990 et 1030, alors on affiche l'image variable.bmp.
    else if(Barometre > 990 && Barometre < 1030){
         m_bitmap1->SetBitmap(wxBitmap(wxImage(_T("image/variable.bmp")).Rescale(wxSize(76,76).GetWidth(),wxSize(60,60).GetHeight())));//variable
    }
    //Si la valeur du barometre en compris entre 1030 et 1080, alors on affiche l'image sun.bmp.
    else if(Barometre > 1030 && Barometre < 1080){
        m_bitmap1->SetBitmap(wxBitmap(wxImage(_T("image/sun.bmp")).Rescale(wxSize(71,71).GetWidth(),wxSize(51,51).GetHeight())));//beau
    }
}

void EvtStationMeteo::LedPortSerieOn()
{
 //On affiche que le thread est en écoute.
    this->m_staticTextEtatPortSerie->SetLabel(wxT("En écoute"));
}

void EvtStationMeteo::LedPortSerieOff(const wxString Etat)
{
//On affiche le message d'erreur.
    this->m_staticTextEtatPortSerie->SetLabel(Etat);
}

void EvtStationMeteo::DirectionDuVent(int angle)
{
    //Si la valeur de l'angle en compris entre 0 et 22, alors on affiche l'image nord.bmp.
    if( 0 < angle && angle < 22){
        m_staticTextDirectL->SetLabel(wxT("Nord"));
         m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/nord.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 22 et 67, alors on affiche l'image nordest.bmp.
    else if(22 < angle && angle < 67){
        m_staticTextDirectL->SetLabel(wxT("Nord Est"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/nordest.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 67 et 122, alors on affiche l'image est.bmp.
    else if(67 < angle && angle < 122){
        m_staticTextDirectL->SetLabel(wxT("Est"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/est.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 112 et 157, alors on affiche l'image sudest.bmp.
    else if(112 < angle && angle < 157){
        m_staticTextDirectL->SetLabel(wxT("Sud Est"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/sudest.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 157 et 202, alors on affiche l'image sud.bmp.
    else if(157 < angle && angle < 202){
        m_staticTextDirectL->SetLabel(wxT("Sud"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/sud.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 202 et 247, alors on affiche l'image sudouest.bmp.
    else if(202 < angle && angle < 247){
        m_staticTextDirectL->SetLabel(wxT("Sud Ouest"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/sudouest.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 247 et 292, alors on affiche l'image ouest.bmp.
    else if(247 < angle && angle < 292){
        m_staticTextDirectL->SetLabel(wxT("Ouest"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/ouest.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 292 et 337, alors on affiche l'image nordouest.bmp.
    else if(292 < angle && angle <337){
        m_staticTextDirectL->SetLabel(wxT("Nord Ouest"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/nordouest.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
    //Sinon la valeur de l'angle en compris entre 337 et 360, alors on affiche l'image nord.bmp.
    else if(337 < angle && angle < 360){
        m_staticTextDirectL->SetLabel(wxT("Nord"));
        m_bitmapbousole->SetBitmap(wxBitmap(wxImage(_T("image/nord.bmp")).Rescale(wxSize(205,205).GetWidth(),wxSize(206,206).GetHeight())));
    }
}

void EvtStationMeteo::OnClickButtonArret( wxCommandEvent& event )
{
    //Si le serveur est allumé on l'éteint.
    if (m_serveur != NULL){
        m_serveur->Close();
        delete m_serveur;
    }

   //Si le thread tourne on l'arrête.
    if(!FinDemandee){
        FinDemandee = true;
    }
  //Et on detruit l'IHM
    Destroy();
}

