///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug 28 2009)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __StationMeteo__
#define __StationMeteo__

#include <wx/sizer.h>
#include <wx/gdicmn.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/combobox.h>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/gauge.h>
#include <wx/notebook.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class StationMeteo
///////////////////////////////////////////////////////////////////////////////
class StationMeteo : public wxFrame 
{
	private:
	
	protected:
		wxNotebook* m_notebook1;
		wxPanel* m_panelStationMeteo;
		wxStaticBitmap* m_bitmapbousole;
		wxStaticText* m_staticTextDirectL;
		wxStaticText* m_staticTextDirect;
		wxStaticText* m_staticTextVitesse;
		wxStaticText* m_staticText14;
		wxStaticText* m_staticText10;
		wxStaticText* m_staticTempExt;
		wxStaticText* m_staticTextExtUnit;
		wxStaticText* m_staticHumExt;
		wxStaticText* m_staticText8;
		wxStaticText* m_staticTempInt;
		wxStaticText* m_staticTextIntUnit;
		wxStaticText* m_staticHumInt;
		wxStaticText* m_staticText81;
		wxStaticText* m_staticTextBaro;
		wxStaticText* m_staticText12;
		wxStaticBitmap* m_bitmap1;
		wxPanel* m_panelconfig;
		wxTextCtrl* m_textCtrlAdress
		;
		wxComboBox* m_comboBoxVitesse;
		wxComboBox* m_comboBoxDb;
		wxComboBox* m_comboBoxPrt;
		wxButton* m_button1;
		wxButton* m_button2;
		wxButton* m_buttonStop;
		wxStaticText* m_staticText1;
		wxStaticText* m_staticTextCnnect;
		wxStaticText* m_staticText63;
		wxStaticText* m_staticTextEtatPortSerie;
		wxButton* m_buttonQuitterPortSerie;
		wxButton* m_buttonRelancerPortSerie;
		wxStaticText* m_staticText62;
		wxSpinCtrl* m_spinCtrlPort;
		wxTextCtrl* m_textCtrlLog;
		wxButton* m_buttonDemarrerServeur;
		wxPanel* m_panelbatterie;
		wxStaticText* m_staticText203;
		wxStaticText* m_staticText2035;
		wxStaticText* m_staticText2034;
		wxStaticText* m_staticText2033;
		wxStaticText* m_staticText2032;
		wxStaticText* m_staticText2031;
		wxStaticText* m_staticText202;
		wxStaticText* m_staticText20;
		wxStaticText* m_staticText201;
		wxStaticText* m_staticText19;
		wxStaticText* m_staticText18;
		wxGauge* m_gaugeInt;
		wxStaticText* m_staticText2036;
		wxStaticText* m_staticText20351;
		wxStaticText* m_staticText20341;
		wxStaticText* m_staticText20331;
		wxStaticText* m_staticText20321;
		wxStaticText* m_staticText20311;
		wxStaticText* m_staticText2021;
		wxStaticText* m_staticText204;
		wxStaticText* m_staticText2011;
		wxStaticText* m_staticText191;
		wxStaticText* m_staticText181;
		wxGauge* m_gaugePluie;
		wxStaticText* m_staticText2037;
		wxStaticText* m_staticText20352;
		wxStaticText* m_staticText20342;
		wxStaticText* m_staticText20332;
		wxStaticText* m_staticText20322;
		wxStaticText* m_staticText20312;
		wxStaticText* m_staticText2022;
		wxStaticText* m_staticText205;
		wxStaticText* m_staticText2012;
		wxStaticText* m_staticText192;
		wxStaticText* m_staticText182;
		wxGauge* m_gaugeGirouette;
		wxStaticText* m_staticText2038;
		wxStaticText* m_staticText20353;
		wxStaticText* m_staticText20343;
		wxStaticText* m_staticText20333;
		wxStaticText* m_staticText20323;
		wxStaticText* m_staticText20313;
		wxStaticText* m_staticText2023;
		wxStaticText* m_staticText206;
		wxStaticText* m_staticText2013;
		wxStaticText* m_staticText193;
		wxStaticText* m_staticText183;
		wxGauge* m_gaugeTempHum;
		wxPanel* m_panelService;
		
		// Virtual event handlers, overide them in your derived class
		virtual void OnFrameClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void GetValueVentDirec( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueVitesse( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueVentVit( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValuePluie( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueExtTemp( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueExtHum( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueIntTemp( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueIntHul( wxMouseEvent& event ) { event.Skip(); }
		virtual void GetValueBaro( wxMouseEvent& event ) { event.Skip(); }
		virtual void OnClickValider( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClickDefaut( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClickButtonArret( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClickButtonQuitter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClickButtonRelancer( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnButtonServeur( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxStaticText* m_staticTextPluie;
		wxTextCtrl* m_textCtrldebug;
		wxTextCtrl* m_textCtrlDebugServeur;
		
		StationMeteo( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Station Météo"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 750,400 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		~StationMeteo();
	
};

#endif //__StationMeteo__
