///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug 28 2009)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "StationMeteo.h"

///////////////////////////////////////////////////////////////////////////

StationMeteo::StationMeteo( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 700,350 ), wxSize( 800,480 ) );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxVERTICAL );
	
	bSizer3->SetMinSize( wxSize( 300,200 ) ); 
	m_notebook1 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_LEFT );
	m_panelStationMeteo = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelStationMeteo->SetBackgroundColour( wxColour( 79, 119, 206 ) );
	m_panelStationMeteo->SetMinSize( wxSize( 300,200 ) );
	m_panelStationMeteo->SetMaxSize( wxSize( 600,400 ) );
	
	wxBoxSizer* bSizerGeneral;
	bSizerGeneral = new wxBoxSizer( wxHORIZONTAL );
	
	bSizerGeneral->SetMinSize( wxSize( 300,200 ) ); 
	wxBoxSizer* bSizer191;
	bSizer191 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizerVent;
	sbSizerVent = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Vent") ), wxVERTICAL );
	
	sbSizerVent->SetMinSize( wxSize( 300,200 ) ); 
	wxStaticBoxSizer* SizerDirecVent;
	SizerDirecVent = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Direction") ), wxVERTICAL );
	
	wxBoxSizer* bSizer135;
	bSizer135 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer141;
	bSizer141 = new wxBoxSizer( wxVERTICAL );
	
	bSizer135->Add( bSizer141, 1, wxEXPAND, 5 );
	
	m_bitmapbousole = new wxStaticBitmap( m_panelStationMeteo, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer135->Add( m_bitmapbousole, 4, wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer142;
	bSizer142 = new wxBoxSizer( wxVERTICAL );
	
	bSizer135->Add( bSizer142, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer143;
	bSizer143 = new wxBoxSizer( wxVERTICAL );
	
	bSizer135->Add( bSizer143, 1, wxEXPAND, 5 );
	
	SizerDirecVent->Add( bSizer135, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer136;
	bSizer136 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer137;
	bSizer137 = new wxBoxSizer( wxVERTICAL );
	
	bSizer136->Add( bSizer137, 1, wxEXPAND, 5 );
	
	m_staticTextDirectL = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("N"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextDirectL->Wrap( -1 );
	m_staticTextDirectL->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer136->Add( m_staticTextDirectL, 0, wxALIGN_CENTER|wxALL, 5 );
	
	wxBoxSizer* bSizer1381;
	bSizer1381 = new wxBoxSizer( wxVERTICAL );
	
	bSizer136->Add( bSizer1381, 1, wxEXPAND, 5 );
	
	m_staticTextDirect = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextDirect->Wrap( -1 );
	m_staticTextDirect->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer136->Add( m_staticTextDirect, 0, wxALL|wxBOTTOM|wxTOP, 5 );
	
	wxBoxSizer* bSizer140;
	bSizer140 = new wxBoxSizer( wxVERTICAL );
	
	bSizer136->Add( bSizer140, 1, wxEXPAND, 5 );
	
	SizerDirecVent->Add( bSizer136, 0, wxEXPAND, 5 );
	
	sbSizerVent->Add( SizerDirecVent, 2, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizerVitVent;
	sbSizerVitVent = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Vitesse") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxHORIZONTAL );
	
	sbSizerVitVent->Add( bSizer13, 1, wxEXPAND|wxLEFT, 5 );
	
	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer139;
	bSizer139 = new wxBoxSizer( wxVERTICAL );
	
	bSizer161->Add( bSizer139, 1, wxEXPAND, 5 );
	
	m_staticTextVitesse = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextVitesse->Wrap( -1 );
	m_staticTextVitesse->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer161->Add( m_staticTextVitesse, 1, wxALIGN_CENTER|wxALL|wxALIGN_RIGHT, 5 );
	
	m_staticText14 = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("m/s"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText14->Wrap( -1 );
	m_staticText14->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer161->Add( m_staticText14, 0, wxALIGN_CENTER|wxALL, 5 );
	
	sbSizerVitVent->Add( bSizer161, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerVitVent->Add( bSizer21, 1, wxEXPAND, 5 );
	
	sbSizerVent->Add( sbSizerVitVent, 0, wxEXPAND, 5 );
	
	bSizer191->Add( sbSizerVent, 2, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizerPluie;
	sbSizerPluie = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Pluie") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );
	
	sbSizerPluie->Add( bSizer15, 1, wxALIGN_CENTER|wxEXPAND|wxLEFT, 5 );
	
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticTextPluie = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextPluie->Wrap( -1 );
	m_staticTextPluie->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), 70, 90, 90, false, wxEmptyString ) );
	m_staticTextPluie->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer19->Add( m_staticTextPluie, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	m_staticText10 = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("mm/hr"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	m_staticText10->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer19->Add( m_staticText10, 0, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	sbSizerPluie->Add( bSizer19, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer138;
	bSizer138 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerPluie->Add( bSizer138, 1, wxEXPAND, 5 );
	
	bSizer191->Add( sbSizerPluie, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	bSizerGeneral->Add( bSizer191, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer31;
	bSizer31 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizerTempExt1;
	sbSizerTempExt1 = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Extérieur") ), wxHORIZONTAL );
	
	wxStaticBoxSizer* sbSizerTempExt;
	sbSizerTempExt = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Température") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer127;
	bSizer127 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempExt->Add( bSizer127, 1, wxEXPAND, 5 );
	
	m_staticTempExt = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTempExt->Wrap( -1 );
	m_staticTempExt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerTempExt->Add( m_staticTempExt, 1, wxALIGN_CENTER|wxALL|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer144;
	bSizer144 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempExt->Add( bSizer144, 1, wxEXPAND, 5 );
	
	m_staticTextExtUnit = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("°C"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextExtUnit->Wrap( -1 );
	m_staticTextExtUnit->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerTempExt->Add( m_staticTextExtUnit, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer133;
	bSizer133 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempExt->Add( bSizer133, 1, wxEXPAND, 5 );
	
	sbSizerTempExt1->Add( sbSizerTempExt, 1, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizerHumExt;
	sbSizerHumExt = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Humidité") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer128;
	bSizer128 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerHumExt->Add( bSizer128, 1, wxEXPAND, 5 );
	
	m_staticHumExt = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticHumExt->Wrap( -1 );
	m_staticHumExt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerHumExt->Add( m_staticHumExt, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer134;
	bSizer134 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerHumExt->Add( bSizer134, 1, wxEXPAND, 5 );
	
	m_staticText8 = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	m_staticText8->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerHumExt->Add( m_staticText8, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer145;
	bSizer145 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerHumExt->Add( bSizer145, 1, wxEXPAND, 5 );
	
	sbSizerTempExt1->Add( sbSizerHumExt, 1, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	bSizer31->Add( sbSizerTempExt1, 1, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizerTempInt;
	sbSizerTempInt = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Intérieur") ), wxHORIZONTAL );
	
	wxStaticBoxSizer* sbSizerTempInt2;
	sbSizerTempInt2 = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Température") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer129;
	bSizer129 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempInt2->Add( bSizer129, 1, wxEXPAND, 5 );
	
	m_staticTempInt = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTempInt->Wrap( -1 );
	m_staticTempInt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerTempInt2->Add( m_staticTempInt, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer146;
	bSizer146 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempInt2->Add( bSizer146, 1, wxEXPAND, 5 );
	
	m_staticTextIntUnit = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("°C"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextIntUnit->Wrap( -1 );
	m_staticTextIntUnit->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerTempInt2->Add( m_staticTextIntUnit, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer130;
	bSizer130 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerTempInt2->Add( bSizer130, 1, wxEXPAND, 5 );
	
	sbSizerTempInt->Add( sbSizerTempInt2, 1, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizerHumInt;
	sbSizerHumInt = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Humidité") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer131;
	bSizer131 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerHumInt->Add( bSizer131, 1, wxEXPAND, 5 );
	
	m_staticHumInt = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticHumInt->Wrap( -1 );
	m_staticHumInt->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerHumInt->Add( m_staticHumInt, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	m_staticText81 = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("%"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText81->Wrap( -1 );
	m_staticText81->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	sbSizerHumInt->Add( m_staticText81, 1, wxALIGN_CENTER|wxALL|wxRIGHT, 5 );
	
	wxBoxSizer* bSizer1321;
	bSizer1321 = new wxBoxSizer( wxVERTICAL );
	
	sbSizerHumInt->Add( bSizer1321, 1, wxEXPAND, 5 );
	
	sbSizerTempInt->Add( sbSizerHumInt, 1, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	bSizer31->Add( sbSizerTempInt, 1, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer10;
	sbSizer10 = new wxStaticBoxSizer( new wxStaticBox( m_panelStationMeteo, wxID_ANY, wxT("Baromètre") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticTextBaro = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("00"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextBaro->Wrap( -1 );
	m_staticTextBaro->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer17->Add( m_staticTextBaro, 1, wxALIGN_CENTER|wxALL|wxRIGHT|wxALIGN_RIGHT, 5 );
	
	wxBoxSizer* bSizer1391;
	bSizer1391 = new wxBoxSizer( wxVERTICAL );
	
	bSizer17->Add( bSizer1391, 0, wxEXPAND, 5 );
	
	m_staticText12 = new wxStaticText( m_panelStationMeteo, wxID_ANY, wxT("mb"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	m_staticText12->SetForegroundColour( wxColour( 255, 255, 255 ) );
	
	bSizer17->Add( m_staticText12, 1, wxALIGN_CENTER|wxALL|wxRIGHT|wxALIGN_RIGHT, 5 );
	
	bSizer16->Add( bSizer17, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer122;
	bSizer122 = new wxBoxSizer( wxVERTICAL );
	
	bSizer18->Add( bSizer122, 1, wxEXPAND, 5 );
	
	m_bitmap1 = new wxStaticBitmap( m_panelStationMeteo, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer18->Add( m_bitmap1, 3, wxALIGN_CENTER|wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer123;
	bSizer123 = new wxBoxSizer( wxVERTICAL );
	
	bSizer18->Add( bSizer123, 1, wxEXPAND, 5 );
	
	bSizer16->Add( bSizer18, 1, wxEXPAND, 5 );
	
	sbSizer10->Add( bSizer16, 1, wxEXPAND, 5 );
	
	bSizer31->Add( sbSizer10, 2, wxEXPAND, 5 );
	
	bSizerGeneral->Add( bSizer31, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_panelStationMeteo->SetSizer( bSizerGeneral );
	m_panelStationMeteo->Layout();
	bSizerGeneral->Fit( m_panelStationMeteo );
	m_notebook1->AddPage( m_panelStationMeteo, wxT("Application"), false );
	m_panelconfig = new wxPanel( m_notebook1, wxID_ANY, wxPoint( 600,400 ), wxSize( 600,400 ), wxTAB_TRAVERSAL );
	m_panelconfig->SetMinSize( wxSize( 150,100 ) );
	
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer2;
	sbSizer2 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Configuration du port série RS-232") ), wxVERTICAL );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer81;
	sbSizer81 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Adresse port série") ), wxVERTICAL );
	
	m_textCtrlAdress
	= new wxTextCtrl( m_panelconfig, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer81->Add( m_textCtrlAdress
	, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	bSizer7->Add( sbSizer81, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxFIXED_MINSIZE, 5 );
	
	wxStaticBoxSizer* sbSizer3;
	sbSizer3 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Vitesse") ), wxVERTICAL );
	
	m_comboBoxVitesse = new wxComboBox( m_panelconfig, wxID_ANY, wxT("Sélection de la vitesse"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboBoxVitesse->Append( wxT("150") );
	m_comboBoxVitesse->Append( wxT("300") );
	m_comboBoxVitesse->Append( wxT("600") );
	m_comboBoxVitesse->Append( wxT("1200") );
	m_comboBoxVitesse->Append( wxT("2400") );
	m_comboBoxVitesse->Append( wxT("4800") );
	m_comboBoxVitesse->Append( wxT("9600") );
	m_comboBoxVitesse->Append( wxT("19200") );
	m_comboBoxVitesse->Append( wxT("38400") );
	m_comboBoxVitesse->Append( wxT("57600") );
	m_comboBoxVitesse->Append( wxT("115200") );
	m_comboBoxVitesse->Append( wxT("230400") );
	m_comboBoxVitesse->Append( wxT("460800") );
	m_comboBoxVitesse->Append( wxT("921600") );
	sbSizer3->Add( m_comboBoxVitesse, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	bSizer7->Add( sbSizer3, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxFIXED_MINSIZE, 5 );
	
	wxStaticBoxSizer* sbSizer4;
	sbSizer4 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Bit de données") ), wxVERTICAL );
	
	m_comboBoxDb = new wxComboBox( m_panelconfig, wxID_ANY, wxT("Selection du nombre de bit"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboBoxDb->Append( wxT("5") );
	m_comboBoxDb->Append( wxT("6") );
	m_comboBoxDb->Append( wxT("7") );
	m_comboBoxDb->Append( wxT("8") );
	sbSizer4->Add( m_comboBoxDb, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	bSizer7->Add( sbSizer4, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxFIXED_MINSIZE, 5 );
	
	wxStaticBoxSizer* sbSizer5;
	sbSizer5 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Parité") ), wxVERTICAL );
	
	m_comboBoxPrt = new wxComboBox( m_panelconfig, wxID_ANY, wxT("NONE"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_READONLY );
	m_comboBoxPrt->Append( wxT("NONE") );
	m_comboBoxPrt->Append( wxT("ODD") );
	m_comboBoxPrt->Append( wxT("EVEN") );
	m_comboBoxPrt->Append( wxT("MARCK") );
	m_comboBoxPrt->Append( wxT("SPACE") );
	sbSizer5->Add( m_comboBoxPrt, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	bSizer7->Add( sbSizer5, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxFIXED_MINSIZE, 5 );
	
	sbSizer2->Add( bSizer7, 1, wxEXPAND, 5 );
	
	bSizer32->Add( sbSizer2, 3, wxALIGN_BOTTOM|wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxHORIZONTAL );
	
	m_button1 = new wxButton( m_panelconfig, wxID_ANY, wxT("Valider"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_button1, 0, wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	m_button2 = new wxButton( m_panelconfig, wxID_ANY, wxT("Valeur par defaut"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer10->Add( m_button2, 0, wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	bSizer32->Add( bSizer10, 0, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	wxBoxSizer* bSizer147;
	bSizer147 = new wxBoxSizer( wxVERTICAL );
	
	m_buttonStop = new wxButton( m_panelconfig, wxID_ANY, wxT("Arreter"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer147->Add( m_buttonStop, 0, wxALIGN_CENTER_HORIZONTAL|wxALL|wxEXPAND, 5 );
	
	bSizer32->Add( bSizer147, 0, wxEXPAND, 5 );
	
	bSizer2->Add( bSizer32, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer8;
	sbSizer8 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Etat Base de données") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer148;
	bSizer148 = new wxBoxSizer( wxVERTICAL );
	
	sbSizer8->Add( bSizer148, 1, wxEXPAND, 5 );
	
	m_staticText1 = new wxStaticText( m_panelconfig, wxID_ANY, wxT("Base de données local : "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	sbSizer8->Add( m_staticText1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_staticTextCnnect = new wxStaticText( m_panelconfig, wxID_ANY, wxT("Non Connecté"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextCnnect->Wrap( -1 );
	sbSizer8->Add( m_staticTextCnnect, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	wxBoxSizer* bSizer1491;
	bSizer1491 = new wxBoxSizer( wxVERTICAL );
	
	sbSizer8->Add( bSizer1491, 1, wxEXPAND, 5 );
	
	bSizer4->Add( sbSizer8, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxStaticBoxSizer* sbSizer251;
	sbSizer251 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Port Série") ), wxVERTICAL );
	
	wxBoxSizer* bSizer125;
	bSizer125 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer149;
	bSizer149 = new wxBoxSizer( wxVERTICAL );
	
	bSizer125->Add( bSizer149, 1, wxEXPAND, 5 );
	
	m_staticText63 = new wxStaticText( m_panelconfig, wxID_ANY, wxT("Etat du port série : "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText63->Wrap( -1 );
	bSizer125->Add( m_staticText63, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_staticTextEtatPortSerie = new wxStaticText( m_panelconfig, wxID_ANY, wxT("Pas de port série"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextEtatPortSerie->Wrap( -1 );
	bSizer125->Add( m_staticTextEtatPortSerie, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxBoxSizer* bSizer150;
	bSizer150 = new wxBoxSizer( wxVERTICAL );
	
	bSizer125->Add( bSizer150, 1, wxEXPAND, 5 );
	
	sbSizer251->Add( bSizer125, 1, wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
	
	wxBoxSizer* bSizer221;
	bSizer221 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer531;
	bSizer531 = new wxBoxSizer( wxVERTICAL );
	
	bSizer221->Add( bSizer531, 1, wxEXPAND, 5 );
	
	m_buttonQuitterPortSerie = new wxButton( m_panelconfig, wxID_ANY, wxT("Quitter"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer221->Add( m_buttonQuitterPortSerie, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer521;
	bSizer521 = new wxBoxSizer( wxVERTICAL );
	
	bSizer221->Add( bSizer521, 1, wxEXPAND, 5 );
	
	m_buttonRelancerPortSerie = new wxButton( m_panelconfig, wxID_ANY, wxT("Relancer"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer221->Add( m_buttonRelancerPortSerie, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer541;
	bSizer541 = new wxBoxSizer( wxVERTICAL );
	
	bSizer221->Add( bSizer541, 1, wxEXPAND, 5 );
	
	sbSizer251->Add( bSizer221, 0, wxEXPAND, 5 );
	
	bSizer4->Add( sbSizer251, 0, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer201;
	sbSizer201 = new wxStaticBoxSizer( new wxStaticBox( m_panelconfig, wxID_ANY, wxT("Etat Serveur") ), wxVERTICAL );
	
	wxBoxSizer* bSizer132;
	bSizer132 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText62 = new wxStaticText( m_panelconfig, wxID_ANY, wxT("Numéro du port"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText62->Wrap( -1 );
	bSizer132->Add( m_staticText62, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	m_spinCtrlPort = new wxSpinCtrl( m_panelconfig, wxID_ANY, wxT("30000"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1025, 65535, 30000 );
	bSizer132->Add( m_spinCtrlPort, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );
	
	sbSizer201->Add( bSizer132, 0, wxEXPAND, 5 );
	
	m_textCtrlLog = new wxTextCtrl( m_panelconfig, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	sbSizer201->Add( m_textCtrlLog, 1, wxALL|wxEXPAND, 5 );
	
	m_buttonDemarrerServeur = new wxButton( m_panelconfig, wxID_ANY, wxT("Démarrer Serveur"), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer201->Add( m_buttonDemarrerServeur, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	bSizer4->Add( sbSizer201, 0, wxEXPAND, 5 );
	
	bSizer2->Add( bSizer4, 1, wxEXPAND, 5 );
	
	m_panelconfig->SetSizer( bSizer2 );
	m_panelconfig->Layout();
	m_notebook1->AddPage( m_panelconfig, wxT("Configuration"), true );
	m_panelbatterie = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticBoxSizer* sbSizer21;
	sbSizer21 = new wxStaticBoxSizer( new wxStaticBox( m_panelbatterie, wxID_ANY, wxT("Module Intérieur") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText203 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("100 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText203->Wrap( -1 );
	bSizer25->Add( m_staticText203, 0, wxALL, 5 );
	
	m_staticText2035 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("90 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2035->Wrap( -1 );
	bSizer25->Add( m_staticText2035, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer33;
	bSizer33 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer33, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer35;
	bSizer35 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer35, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer36;
	bSizer36 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer36, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer34;
	bSizer34 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer34, 1, wxEXPAND, 5 );
	
	m_staticText2034 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("80 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2034->Wrap( -1 );
	bSizer25->Add( m_staticText2034, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer43;
	bSizer43 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer43, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer42, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer41, 1, wxEXPAND, 5 );
	
	m_staticText2033 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("70 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2033->Wrap( -1 );
	bSizer25->Add( m_staticText2033, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer46;
	bSizer46 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer46, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer40;
	bSizer40 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer40, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer39;
	bSizer39 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer39, 1, wxEXPAND, 5 );
	
	m_staticText2032 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("60 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2032->Wrap( -1 );
	bSizer25->Add( m_staticText2032, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer45;
	bSizer45 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer45, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer44;
	bSizer44 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer44, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer38, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer37;
	bSizer37 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer37, 1, wxEXPAND, 5 );
	
	m_staticText2031 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("50 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2031->Wrap( -1 );
	bSizer25->Add( m_staticText2031, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer50;
	bSizer50 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer50, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer49;
	bSizer49 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer49, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer48;
	bSizer48 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer48, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer47;
	bSizer47 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer47, 1, wxEXPAND, 5 );
	
	m_staticText202 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("40 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText202->Wrap( -1 );
	bSizer25->Add( m_staticText202, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer57;
	bSizer57 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer57, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer56;
	bSizer56 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer56, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer55;
	bSizer55 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer55, 1, wxEXPAND, 5 );
	
	m_staticText20 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("30 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	bSizer25->Add( m_staticText20, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer58;
	bSizer58 = new wxBoxSizer( wxVERTICAL );
	
	bSizer25->Add( bSizer58, 1, wxEXPAND, 5 );
	
	m_staticText201 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("20 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText201->Wrap( -1 );
	bSizer25->Add( m_staticText201, 0, wxALL, 5 );
	
	m_staticText19 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("10 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText19->Wrap( -1 );
	bSizer25->Add( m_staticText19, 0, wxALL, 5 );
	
	m_staticText18 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("0 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer25->Add( m_staticText18, 0, wxALL, 5 );
	
	sbSizer21->Add( bSizer25, 1, wxEXPAND, 5 );
	
	m_gaugeInt = new wxGauge( m_panelbatterie, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_VERTICAL );
	m_gaugeInt->SetValue( 10 ); 
	sbSizer21->Add( m_gaugeInt, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer26;
	bSizer26 = new wxBoxSizer( wxVERTICAL );
	
	sbSizer21->Add( bSizer26, 1, wxEXPAND, 5 );
	
	bSizer24->Add( sbSizer21, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizer22;
	sbSizer22 = new wxStaticBoxSizer( new wxStaticBox( m_panelbatterie, wxID_ANY, wxT("Module Pluie") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer251;
	bSizer251 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2036 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("100 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2036->Wrap( -1 );
	bSizer251->Add( m_staticText2036, 0, wxALL, 5 );
	
	m_staticText20351 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("90 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20351->Wrap( -1 );
	bSizer251->Add( m_staticText20351, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer331;
	bSizer331 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer331, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer351;
	bSizer351 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer351, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer361;
	bSizer361 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer361, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer341;
	bSizer341 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer341, 1, wxEXPAND, 5 );
	
	m_staticText20341 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("80 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20341->Wrap( -1 );
	bSizer251->Add( m_staticText20341, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer431;
	bSizer431 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer431, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer421;
	bSizer421 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer421, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer411;
	bSizer411 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer411, 1, wxEXPAND, 5 );
	
	m_staticText20331 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("70 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20331->Wrap( -1 );
	bSizer251->Add( m_staticText20331, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer461;
	bSizer461 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer461, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer401;
	bSizer401 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer401, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer391;
	bSizer391 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer391, 1, wxEXPAND, 5 );
	
	m_staticText20321 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("60 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20321->Wrap( -1 );
	bSizer251->Add( m_staticText20321, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer451;
	bSizer451 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer451, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer441;
	bSizer441 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer441, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer381;
	bSizer381 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer381, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer371;
	bSizer371 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer371, 1, wxEXPAND, 5 );
	
	m_staticText20311 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("50 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20311->Wrap( -1 );
	bSizer251->Add( m_staticText20311, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer501;
	bSizer501 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer501, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer491;
	bSizer491 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer491, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer481;
	bSizer481 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer481, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer471;
	bSizer471 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer471, 1, wxEXPAND, 5 );
	
	m_staticText2021 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("40 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2021->Wrap( -1 );
	bSizer251->Add( m_staticText2021, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer571;
	bSizer571 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer571, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer561;
	bSizer561 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer561, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer551;
	bSizer551 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer551, 1, wxEXPAND, 5 );
	
	m_staticText204 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("30 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText204->Wrap( -1 );
	bSizer251->Add( m_staticText204, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer581;
	bSizer581 = new wxBoxSizer( wxVERTICAL );
	
	bSizer251->Add( bSizer581, 1, wxEXPAND, 5 );
	
	m_staticText2011 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("20 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2011->Wrap( -1 );
	bSizer251->Add( m_staticText2011, 0, wxALL, 5 );
	
	m_staticText191 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("10 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText191->Wrap( -1 );
	bSizer251->Add( m_staticText191, 0, wxALL, 5 );
	
	m_staticText181 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("0 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText181->Wrap( -1 );
	bSizer251->Add( m_staticText181, 0, wxALL, 5 );
	
	sbSizer22->Add( bSizer251, 1, wxEXPAND, 5 );
	
	m_gaugePluie = new wxGauge( m_panelbatterie, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_VERTICAL );
	sbSizer22->Add( m_gaugePluie, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxHORIZONTAL );
	
	sbSizer22->Add( bSizer28, 1, wxEXPAND, 5 );
	
	bSizer24->Add( sbSizer22, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizer23;
	sbSizer23 = new wxStaticBoxSizer( new wxStaticBox( m_panelbatterie, wxID_ANY, wxT("Module Girouette") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer252;
	bSizer252 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2037 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("100 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2037->Wrap( -1 );
	bSizer252->Add( m_staticText2037, 0, wxALL, 5 );
	
	m_staticText20352 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("90 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20352->Wrap( -1 );
	bSizer252->Add( m_staticText20352, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer332;
	bSizer332 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer332, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer352;
	bSizer352 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer352, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer362;
	bSizer362 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer362, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer342;
	bSizer342 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer342, 1, wxEXPAND, 5 );
	
	m_staticText20342 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("80 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20342->Wrap( -1 );
	bSizer252->Add( m_staticText20342, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer432;
	bSizer432 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer432, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer422;
	bSizer422 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer422, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer412;
	bSizer412 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer412, 1, wxEXPAND, 5 );
	
	m_staticText20332 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("70 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20332->Wrap( -1 );
	bSizer252->Add( m_staticText20332, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer462;
	bSizer462 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer462, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer402;
	bSizer402 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer402, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer392;
	bSizer392 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer392, 1, wxEXPAND, 5 );
	
	m_staticText20322 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("60 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20322->Wrap( -1 );
	bSizer252->Add( m_staticText20322, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer452;
	bSizer452 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer452, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer442;
	bSizer442 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer442, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer382;
	bSizer382 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer382, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer372;
	bSizer372 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer372, 1, wxEXPAND, 5 );
	
	m_staticText20312 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("50 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20312->Wrap( -1 );
	bSizer252->Add( m_staticText20312, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer502;
	bSizer502 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer502, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer492;
	bSizer492 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer492, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer482;
	bSizer482 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer482, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer472;
	bSizer472 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer472, 1, wxEXPAND, 5 );
	
	m_staticText2022 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("40 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2022->Wrap( -1 );
	bSizer252->Add( m_staticText2022, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer572;
	bSizer572 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer572, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer562;
	bSizer562 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer562, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer552;
	bSizer552 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer552, 1, wxEXPAND, 5 );
	
	m_staticText205 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("30 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText205->Wrap( -1 );
	bSizer252->Add( m_staticText205, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer582;
	bSizer582 = new wxBoxSizer( wxVERTICAL );
	
	bSizer252->Add( bSizer582, 1, wxEXPAND, 5 );
	
	m_staticText2012 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("20 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2012->Wrap( -1 );
	bSizer252->Add( m_staticText2012, 0, wxALL, 5 );
	
	m_staticText192 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("10 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText192->Wrap( -1 );
	bSizer252->Add( m_staticText192, 0, wxALL, 5 );
	
	m_staticText182 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("0 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText182->Wrap( -1 );
	bSizer252->Add( m_staticText182, 0, wxALL, 5 );
	
	sbSizer23->Add( bSizer252, 1, wxEXPAND, 5 );
	
	m_gaugeGirouette = new wxGauge( m_panelbatterie, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_VERTICAL );
	sbSizer23->Add( m_gaugeGirouette, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer( wxVERTICAL );
	
	sbSizer23->Add( bSizer30, 1, wxEXPAND, 5 );
	
	bSizer24->Add( sbSizer23, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxStaticBoxSizer* sbSizer24;
	sbSizer24 = new wxStaticBoxSizer( new wxStaticBox( m_panelbatterie, wxID_ANY, wxT("Module Temp-Humidite") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer253;
	bSizer253 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2038 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("100 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2038->Wrap( -1 );
	bSizer253->Add( m_staticText2038, 0, wxALL, 5 );
	
	m_staticText20353 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("90 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20353->Wrap( -1 );
	bSizer253->Add( m_staticText20353, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer333;
	bSizer333 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer333, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer353;
	bSizer353 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer353, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer363;
	bSizer363 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer363, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer343;
	bSizer343 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer343, 1, wxEXPAND, 5 );
	
	m_staticText20343 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("80 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20343->Wrap( -1 );
	bSizer253->Add( m_staticText20343, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer433;
	bSizer433 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer433, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer423;
	bSizer423 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer423, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer413;
	bSizer413 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer413, 1, wxEXPAND, 5 );
	
	m_staticText20333 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("70 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20333->Wrap( -1 );
	bSizer253->Add( m_staticText20333, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer463;
	bSizer463 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer463, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer403;
	bSizer403 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer403, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer393;
	bSizer393 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer393, 1, wxEXPAND, 5 );
	
	m_staticText20323 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("60 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20323->Wrap( -1 );
	bSizer253->Add( m_staticText20323, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer453;
	bSizer453 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer453, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer443;
	bSizer443 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer443, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer383;
	bSizer383 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer383, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer373;
	bSizer373 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer373, 1, wxEXPAND, 5 );
	
	m_staticText20313 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("50 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20313->Wrap( -1 );
	bSizer253->Add( m_staticText20313, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer503;
	bSizer503 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer503, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer493;
	bSizer493 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer493, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer483;
	bSizer483 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer483, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer473;
	bSizer473 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer473, 1, wxEXPAND, 5 );
	
	m_staticText2023 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("40 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2023->Wrap( -1 );
	bSizer253->Add( m_staticText2023, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer573;
	bSizer573 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer573, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer563;
	bSizer563 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer563, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer553;
	bSizer553 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer553, 1, wxEXPAND, 5 );
	
	m_staticText206 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("30 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText206->Wrap( -1 );
	bSizer253->Add( m_staticText206, 0, wxALL, 5 );
	
	wxBoxSizer* bSizer583;
	bSizer583 = new wxBoxSizer( wxVERTICAL );
	
	bSizer253->Add( bSizer583, 1, wxEXPAND, 5 );
	
	m_staticText2013 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("20 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2013->Wrap( -1 );
	bSizer253->Add( m_staticText2013, 0, wxALL, 5 );
	
	m_staticText193 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("10 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText193->Wrap( -1 );
	bSizer253->Add( m_staticText193, 0, wxALL, 5 );
	
	m_staticText183 = new wxStaticText( m_panelbatterie, wxID_ANY, wxT("0 %"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText183->Wrap( -1 );
	bSizer253->Add( m_staticText183, 0, wxALL, 5 );
	
	sbSizer24->Add( bSizer253, 1, wxEXPAND, 5 );
	
	m_gaugeTempHum = new wxGauge( m_panelbatterie, wxID_ANY, 100, wxDefaultPosition, wxDefaultSize, wxGA_VERTICAL );
	sbSizer24->Add( m_gaugeTempHum, 0, wxALL|wxEXPAND, 5 );
	
	wxBoxSizer* bSizer321;
	bSizer321 = new wxBoxSizer( wxVERTICAL );
	
	sbSizer24->Add( bSizer321, 1, wxEXPAND, 5 );
	
	bSizer24->Add( sbSizer24, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_panelbatterie->SetSizer( bSizer24 );
	m_panelbatterie->Layout();
	bSizer24->Fit( m_panelbatterie );
	m_notebook1->AddPage( m_panelbatterie, wxT("Batterie"), false );
	m_panelService = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelService->Hide();
	
	wxBoxSizer* bSizer51;
	bSizer51 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer39;
	sbSizer39 = new wxStaticBoxSizer( new wxStaticBox( m_panelService, wxID_ANY, wxT("Debug port serie") ), wxVERTICAL );
	
	m_textCtrldebug = new wxTextCtrl( m_panelService, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	sbSizer39->Add( m_textCtrldebug, 2, wxALL|wxEXPAND, 5 );
	
	bSizer51->Add( sbSizer39, 1, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer40;
	sbSizer40 = new wxStaticBoxSizer( new wxStaticBox( m_panelService, wxID_ANY, wxT("Debug Client-Serveur") ), wxVERTICAL );
	
	m_textCtrlDebugServeur = new wxTextCtrl( m_panelService, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	sbSizer40->Add( m_textCtrlDebugServeur, 0, wxALL|wxEXPAND, 5 );
	
	bSizer51->Add( sbSizer40, 0, wxEXPAND, 5 );
	
	m_panelService->SetSizer( bSizer51 );
	m_panelService->Layout();
	bSizer51->Fit( m_panelService );
	m_notebook1->AddPage( m_panelService, wxT("Debug"), false );
	
	bSizer3->Add( m_notebook1, 1, wxEXPAND | wxALL, 5 );
	
	this->SetSizer( bSizer3 );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( StationMeteo::OnFrameClose ) );
	m_staticTextDirectL->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentDirec ), NULL, this );
	m_staticTextDirect->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentDirec ), NULL, this );
	m_staticTextVitesse->Connect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( StationMeteo::GetValueVitesse ), NULL, this );
	m_staticTextVitesse->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentVit ), NULL, this );
	m_staticTextPluie->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValuePluie ), NULL, this );
	m_staticTempExt->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueExtTemp ), NULL, this );
	m_staticHumExt->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueExtHum ), NULL, this );
	m_staticTempInt->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueIntTemp ), NULL, this );
	m_staticHumInt->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueIntHul ), NULL, this );
	m_staticTextBaro->Connect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueBaro ), NULL, this );
	m_button1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickValider ), NULL, this );
	m_button2->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickDefaut ), NULL, this );
	m_buttonStop->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonArret ), NULL, this );
	m_buttonQuitterPortSerie->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonQuitter ), NULL, this );
	m_buttonRelancerPortSerie->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonRelancer ), NULL, this );
	m_buttonDemarrerServeur->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnButtonServeur ), NULL, this );
}

StationMeteo::~StationMeteo()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( StationMeteo::OnFrameClose ) );
	m_staticTextDirectL->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentDirec ), NULL, this );
	m_staticTextDirect->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentDirec ), NULL, this );
	m_staticTextVitesse->Disconnect( wxEVT_LEFT_DCLICK, wxMouseEventHandler( StationMeteo::GetValueVitesse ), NULL, this );
	m_staticTextVitesse->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueVentVit ), NULL, this );
	m_staticTextPluie->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValuePluie ), NULL, this );
	m_staticTempExt->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueExtTemp ), NULL, this );
	m_staticHumExt->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueExtHum ), NULL, this );
	m_staticTempInt->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueIntTemp ), NULL, this );
	m_staticHumInt->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueIntHul ), NULL, this );
	m_staticTextBaro->Disconnect( wxEVT_LEFT_DOWN, wxMouseEventHandler( StationMeteo::GetValueBaro ), NULL, this );
	m_button1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickValider ), NULL, this );
	m_button2->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickDefaut ), NULL, this );
	m_buttonStop->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonArret ), NULL, this );
	m_buttonQuitterPortSerie->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonQuitter ), NULL, this );
	m_buttonRelancerPortSerie->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnClickButtonRelancer ), NULL, this );
	m_buttonDemarrerServeur->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( StationMeteo::OnButtonServeur ), NULL, this );
}
