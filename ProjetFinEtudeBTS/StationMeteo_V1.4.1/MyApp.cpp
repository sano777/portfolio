/***************************************************************
 * Name:      ConfigurationStationMeteo.cpp
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

 /************************************************************/
 #ifdef WX_PRECOMP
 #include "wx_pch.h"
 #endif
 /************************************************************/

 /************************************************************/
 #ifdef __BORLANDC__
 #pragma hdrstop
 #endif //__BORLANDC__
 /************************************************************/

///////////////////////////////////////////////////////////////
#include "MyApp.h"
#include "ihm/EvtStationMeteo.h"
//////////////////////////////////////////////////////////////

IMPLEMENT_APP(MyApp);

bool MyApp::OnInit()
{
    EvtStationMeteo * frame_principal = new EvtStationMeteo(0);
    frame_principal->Show();

    return true;
}
