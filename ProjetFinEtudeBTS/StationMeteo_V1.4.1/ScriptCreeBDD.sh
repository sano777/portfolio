#!/bin/bash

sqlite3 stationmeteo.db "create table data(key INTEGER PRIMARY KEY AUTOINCREMENT, id INT, date DATE, resultat FLOAT);"
sqlite3 stationmeteo.db "create table capteurs(key INTEGER PRIMARY KEY AUTOINCREMENT,id INT,capteur Text,description Text);"


#sqlite3 stationmeteo.db "insert into data (id,date,resultat) values (1,DATETIME('NOW', 'localtime'), 26);"
#sqlite3 stationmeteo.db "insert into data (id,date,resultat) values (1,DATETIME('NOW', 'localtime'), 22);"
#sqlite3 stationmeteo.db "insert into data (id,date,resultat) values (1,DATETIME('NOW', 'localtime'), 15);"
#sqlite3 stationmeteo.db "insert into data (id,date,resultat) values (1,DATETIME('NOW', 'localtime'), 5);"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (1,'inttemp','Capteur de température intérieur');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (2,'inthygro','Capteur de l humidité intérieur');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (3,'intbaro','Capteur de pression intérieur');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (4,'exttemp','Capteur de température extérieur');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (5,'exthygro','Capteur de l humidité extérieur');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (6,'extanemo','Capteur de vitesse du vent');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (7,'extgirouette','Capteur de direction du vent');"
#sqlite3 stationmeteo.db "insert into capteurs(id,capteur,description) values (8,'extpluvio','Capteur de pluviomètrie');"
#SELECT date,resultat FROM data,capteurs WHERE date>'2010-05-03 15:51:04' AND capteurs.id=data.id^Cnd capteurs.capteur='inthygro' ;
