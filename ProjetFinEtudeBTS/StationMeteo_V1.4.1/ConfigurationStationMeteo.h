/***************************************************************
 * Name:      ConfigurationStationMeteo.h
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

/*************************************************************/
 #ifndef CONFIGURATIONSTATIONMETEO_H
 #define CONFIGURATIONSTATIONMETEO_H
/************************************************************/

/////////////////////////////////////////////////////////////
 #include <wx/xrc/xmlres.h>
 #include <wx/dir.h>
 #include <wx/string.h>
 #include <wx/wx.h>
 #include <wx/textfile.h>
///////////////////////////////////////////////////////////

 #define NAME_CONFIG_FILE wxT("ConfigStationMeteo.xml")


class ConfigurationStationMeteo
{
    public:
        wxString GetPort();
        wxString GetParite();
        wxString GetVitesse();
        wxString GetAdressPort();
        wxString GetBitDonnees();
        void SetPort(wxString port);
        ConfigurationStationMeteo();
        bool RecupereConfiguration();
        bool ModifierConfiguration();
        void SetParite(wxString partie);
        void SetVitesse(wxString vitesse);
        void SetBitDonnees(wxString bitdonnees);
        void SetAdressPort(wxString adressport);
        virtual ~ConfigurationStationMeteo();

    private:
        wxString SParite;
        wxString Vitesse;
        wxString BitDonnees;
        wxString Adresse_Port;
        wxString Port_serveur;
        bool CreeFichierConfig();
        wxXmlDocument m_configuration;
        bool SauveConfiguration(const wxXmlDocument xml_doc);
        bool LitConfiguration(const wxXmlDocument xml_doc);
};

#endif // CONFIGURATIONSTATIONMETEO_H
