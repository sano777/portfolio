/***************************************************************
 * Name:      MyApp.h
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

 /************************************************************/
 #ifndef MYAPP_H
 #define MYAPP_H
/************************************************************/

/////////////////////////////////////////////////////////////
#include <wx/app.h>
////////////////////////////////////////////////////////////

class MyApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // MYAPP_H
