/***************************************************************
 * Name:      ConfigurationStationMeteo.cpp
 * Author:    Sébastien Marie (sebastien.marie07@gmail.com)
 * Created:   2010-05-10
 * Copyright: Sébastien Marie
 * License:    GPL
 **************************************************************/

 //////////////////////////////////////////////////////////////
 #include "ConfigurationStationMeteo.h"
//////////////////////////////////////////////////////////////

ConfigurationStationMeteo::ConfigurationStationMeteo()
{
    // On vérifie si le fichier de config existe.
    if (!wxFile::Exists(NAME_CONFIG_FILE)){
   		wxLogWarning (_("Le fichie de configuration %s n'existe pas, création d'un fichier par défaut"),NAME_CONFIG_FILE);
   		if (!CreeFichierConfig()){
            wxLogWarning (wxT("Le fichier ne peut pas être crée, voir si il y a les droits d'écriture"));
   		}
    }

}

ConfigurationStationMeteo::~ConfigurationStationMeteo()
{
    //dtor
}

bool ConfigurationStationMeteo::CreeFichierConfig()
{
        wxXmlDocument configuration;

        //On crée la parent Confiuration.
        wxXmlNode *root = new wxXmlNode(wxXML_ELEMENT_NODE,wxT("CONFIGURATION"));
        configuration.SetRoot(root);

        //On crée le fils Port avec sa valeur.
        wxXmlNode *PORT = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("PORT"));
        PORT->AddProperty(wxT("Numero"), wxT("30000"));
        root->AddChild(PORT);

        //On crée le fils Adresse Port avec sa valeur.
        wxXmlNode *Adress = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("ADRESSE_PORT"));
        Adress->AddProperty(wxT("Adresse"), wxT("/dev/USB0"));
        root->AddChild(Adress);

        //On crée le fils Vitesse avec sa valeur.
        wxXmlNode *Vitesse = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("Vitesse"));
        Vitesse->AddProperty(wxT("Vitesse"), wxT("57600"));
        root->AddChild(Vitesse);

        //On crée le fils BitDonnées avec sa valeur.
        wxXmlNode *BitDonnees = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("BitDonnees"));
        BitDonnees->AddProperty(wxT("BitDonnees"), wxT("8"));
        root->AddChild(BitDonnees );

        //On crée le fils Parité avec sa valeur.
        wxXmlNode *Parite = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("Parite"));
        Parite->AddProperty(wxT("Parite"), wxT("NONE"));
        root->AddChild(Parite);

        //On sauvegarde la configuration, si il n'a pas d'erreur on recharge le document XML.
        if (SauveConfiguration(configuration)){
        //Si on arrive pas a recharger le fichier on avertie l'utilisateur.
            if (!m_configuration.Load (NAME_CONFIG_FILE, wxT ("UTF-8"))){
                wxLogWarning (_("Impossible de charger le fichier de configuration par défaut %s"),NAME_CONFIG_FILE);
                return false;
            }
            else return true;
        }
        //Si on ne peut pas sauvegarde, on avertie l'utilisateur.
        else{
            wxLogWarning (_("Impossible de créer le fichier de configuration par défaut %s"),NAME_CONFIG_FILE);
            return false;
        }
}

bool ConfigurationStationMeteo::SauveConfiguration(const wxXmlDocument xml_doc)
{
    const wxString nom_fichier_conf = NAME_CONFIG_FILE;
	//On sauve le fichier de configuration, si il y a une erreur on avertir l'utilisateur.
	if (!xml_doc.Save (nom_fichier_conf, 2)){
		wxLogWarning (_("Problème d'enregistrement avec le fichier: %s"),nom_fichier_conf.c_str());
		return false;
	}
	else{
	    //On fixe le type de terminaison du fichier en fonction de l'OS
	    wxTextFile config;
	    config.Open(nom_fichier_conf);
        #if defined (__WXMSW__)
        config.Write (wxTextFileType_Dos);
        #elif defined (__LINUX__)
        config.Write (wxTextFileType_Unix);
        #endif
        return true;
	}


}

bool ConfigurationStationMeteo::RecupereConfiguration ()
{

	//On ouvre le fichier de configuration (vérification de la validité XML)
	if (!m_configuration.Load (NAME_CONFIG_FILE, wxT ("UTF-8"))){
	    //Si le fichier XML n'est pas valide, on en créé un nouveau
		wxLogWarning (_("Le fichier de configuration %s contient des erreurs, création d'un fichier par défaut."),NAME_CONFIG_FILE);
        if (!CreeFichierConfig())
                return false;
	}
	//On vérifie si la racine du fichier de conf est bien nommée 'CONFIGURATION'
	if (m_configuration.GetRoot ()->GetName () != wxT("CONFIGURATION")){
	    //Si le fichier XML n'est pas valide, on en créé un nouveau
		wxLogWarning (_("Le fichier de configuration %s contient des erreurs, création d'un fichier par défaut."),NAME_CONFIG_FILE);
        if (!CreeFichierConfig())
                return false;
	}
    //On essaye de charger la configuration demandée
    if (!LitConfiguration(m_configuration)){
        if (!CreeFichierConfig())
                return false;

        else
            return LitConfiguration(m_configuration);
    }
    else return true;
}

bool ConfigurationStationMeteo::LitConfiguration(const wxXmlDocument xml_doc)
{
	wxXmlNode *child = xml_doc.GetRoot ()->GetChildren ();
	//On parcourt les différentes entrées.
    while (child)
    {
        if (child->GetName () == wxT ("PORT"))
        {
            //Récupération de la valeur entourée par <PORT></PORT>.
             Port_serveur = child->GetPropVal (wxT ("Numero"), wxT ("30000"));
        }
        else if (child->GetName () == wxT ("ADRESSE_PORT"))
        {
            //Récupération de la valeur entourée par <ADRESSE_PORT></ADRESSE_PORT>.
            Adresse_Port = child->GetPropVal (wxT ("Adresse"), wxT ("/dev/USB0"));
        }
        else if (child->GetName () == wxT ("Vitesse"))
        {
            //Récupération de la valeur entourée par <ADRESSE_HOTE></ADRESSE_HOTE>.
            Vitesse  = child->GetPropVal (wxT ("Vitesse"), wxT ("57600"));

        }
        else if (child->GetName () == wxT ("BitDonnees"))
        {
            //Récupération de la valeur entourée par <ADRESSE_HOTE></ADRESSE_HOTE>.
            BitDonnees = child->GetPropVal (wxT ("BitDonnees"), wxT ("8"));
        }
        else if (child->GetName () == wxT ("Parite"))
        {
            //Récupération de la valeur entourée par <ADRESSE_HOTE></ADRESSE_HOTE>.
            SParite = child->GetPropVal (wxT ("Parite"), wxT ("NONE"));
        }
        child = child->GetNext ();
    }

    return true;
}

bool ConfigurationStationMeteo::ModifierConfiguration()
{
    wxXmlDocument configuration;

    wxString SPort = wxT("");
    SPort<<Port_serveur;

    wxString SVitesse = wxT("");
    SVitesse<<Vitesse;

    wxString SBitDonnees = wxT("");
    SBitDonnees<<BitDonnees;

    wxXmlNode *root = new wxXmlNode(wxXML_ELEMENT_NODE,wxT("CONFIGURATION"));
    configuration.SetRoot(root);

    wxXmlNode *PORT = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("PORT"));
    PORT->AddProperty(wxT("Numero"), SPort);
    root->AddChild(PORT);

    wxXmlNode *Adress = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("ADRESSE_PORT"));
    Adress->AddProperty(wxT("Adresse"), Adresse_Port);
    root->AddChild(Adress);

    wxXmlNode *Vitesse = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("Vitesse"));
    Vitesse->AddProperty(wxT("Vitesse"), SVitesse);
    root->AddChild(Vitesse);

    wxXmlNode *BitDonnees = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("BitDonnees"));
    BitDonnees->AddProperty(wxT("BitDonnees"), SBitDonnees);
    root->AddChild(BitDonnees );

    wxXmlNode *Parite = new wxXmlNode(wxXML_ELEMENT_NODE, wxT("Parite"));
    Parite->AddProperty(wxT("Parite"), SParite);
    root->AddChild(Parite);

    //On sauvegarde la configuration, si il n'a pas d'erreur on recharge le document XML.
    if (SauveConfiguration(configuration)){
    //Si on arrive pas a recharger le fichier on avertie l'utilisateur.
       if (!m_configuration.Load (NAME_CONFIG_FILE, wxT ("UTF-8"))){
           wxLogWarning (_("Impossible de charger le fichier de configuration par défaut %s"),NAME_CONFIG_FILE);
           return false;
       }
       else return true;
    }
    //Si on ne peut pas sauvegarde, on avertie l'utilisateur.
    else{
       wxLogWarning (_("Impossible de créer le fichier de configuration par défaut %s"),NAME_CONFIG_FILE);
       return false;
    }
}

wxString ConfigurationStationMeteo::GetPort()
{
    return Port_serveur;
}

wxString ConfigurationStationMeteo::GetVitesse()
{
    return Vitesse;
}

wxString ConfigurationStationMeteo::GetBitDonnees()
{
    return BitDonnees;
}

wxString ConfigurationStationMeteo::GetAdressPort()
{
    return Adresse_Port;
}

wxString ConfigurationStationMeteo::GetParite()
{
    return SParite;
}

void ConfigurationStationMeteo::SetPort(wxString port)
{
        Port_serveur = port;
}

void ConfigurationStationMeteo::SetVitesse(wxString vitesse)
{
        Vitesse = vitesse;
}

void ConfigurationStationMeteo::SetBitDonnees(wxString bitdonnees)
{
        BitDonnees = bitdonnees;
}

void ConfigurationStationMeteo::SetAdressPort(wxString adressport)
{
        Adresse_Port = adressport;
}

void ConfigurationStationMeteo::SetParite(wxString parite)
{
        SParite = parite;
}
