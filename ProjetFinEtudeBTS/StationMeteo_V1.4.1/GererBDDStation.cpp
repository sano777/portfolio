/***************************************************************
 * Name:      GererBDDStation.cpp
 * Auteur:    Clément DEFAUX
 **************************************************************/
#include "GererBDDStation.h"
#include <sqlite3.h>
#include <wx/datetime.h>
#include <wx/file.h>


using namespace std;
wxString GererBDDStation::trame=wxT("");
wxString GererBDDStation::DateMax=wxT("");


GererBDDStation::GererBDDStation( EvtStationMeteo * Frame)
{
    FrameStation=Frame;
    if(wxFile::Exists(wxT("stationmeteo.db")))
    {

    if(sqlite3_open("stationmeteo.db", &SQLite ) == SQLITE_OK)
    {
        //Connexion réussie
        FrameStation->m_textCtrldebug->AppendText(wxT("connexion à la BDD réussie \n"));
        Etat=1;
    }
    else
    {
        //échec de la connexion
        wxLogWarning(wxT("Echec de connexion à la BDD"));
        FrameStation->m_textCtrldebug->AppendText(wxT("Echec de connexion à la BDD \n"));
        Etat=0;
    }
    }
    else
    {
        wxLogWarning(wxT("Veillez à ce que stationmeteo.db existe puis redémarrez le programme"));
        Etat=0;
    }
}

GererBDDStation::~GererBDDStation()
{
    //déconnexion de la base de données
    sqlite3_close(SQLite);
}


//fonction appelée à chaque ligne de la requete select
int GererBDDStation::callback(void *NotUsed, int argc, char **argv, char **azColName)
{
            NotUsed=0;
            int i;
            char * Valeur;
            char * NomColone;

            for(i=0; i<argc; i++)
            {
                Valeur=argv[i];
                wxString Val(Valeur,wxConvUTF8);
                NomColone=azColName[i];
                wxString NomCol(NomColone,wxConvUTF8);
                //Mise en forme de la trame
                if(NomCol==wxT("date"))
                {
                    //Vérification du format de date : AAAA-MM-JJ hh:mm:ss
                    wxString Annee,Jour,Mois,Time;
                    if( Val.GetChar(2)==wxT('/') && Val.GetChar(5)==wxT('/') && Val.GetChar(10)==wxT(' ') && Val.GetChar(13)==wxT(':') && Val.GetChar(16)==wxT(':') )
                    {
                        Time=Val.SubString(11,18);
                        Annee=Val.SubString(6,9);
                        Jour=Val.SubString(0,1);
                        Mois=Val.SubString(3,4);
                        Val=Annee+wxT("-")+Mois+wxT("-")+Jour+wxT(" ")+Time;
                    }
                    else if( Val.GetChar(4)==wxT('-') && Val.GetChar(7)==wxT('-') && Val.GetChar(10)==wxT(' ') && Val.GetChar(13)==wxT(':') && Val.GetChar(16)==wxT(':') )
                    {;}
                    else
                    {
                        Val=wxT("erreur");
                    }
                    if(DateMax>Val)
                    {
                        return 0;
                    }
                    trame=trame+Val+wxT("/");
                }
                if(NomCol==wxT("resultat"))
                {
                    trame=trame+Val+wxT(";");
                }
            }
            printf("\n");
            return 0;

}


//Retourne toutes les données non synchronisé à partir d'une date pour un capteur
wxString GererBDDStation::LitBDDSynchro(wxString Capteur,wxString Date)
{
    if(Etat==1)
    {
    int    Res;
    int    NbrColone=0;
    int    NbrLigne=0;
    char **Resultat;
    int    rc;
    char * zErrMsg;
    wxString chaine,Requete,Requete1,NumCap,DateCorrecte;
    DateMax=Date;

    //Mise à 0 de la trame
    trame.Clear();
    DateCorrecte=VerifieDate(Date);
    //On récupere le numéro du capteur
    Requete1=wxT("SELECT id FROM capteurs WHERE capteur='")+Capteur+wxT("' ;");
    if(sqlite3_get_table(SQLite, Requete1.mb_str(wxConvUTF8), &Resultat,&NbrLigne,&NbrColone, &zErrMsg) == SQLITE_OK)
    {
        if(NbrLigne==0)
       {
           FrameStation->m_textCtrlDebugServeur->AppendText(wxT("erreur"));
       }
       else
       {
           Res = atoi(Resultat[1]);
           NumCap<<Res;
       }
    }
    else
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }

    //Début de la trame
    trame<<NumCap;
    trame=trame+wxT("01+");
    //Mise en forme de la requete, récupere les données du capteur NumCap apres la date indiqué
    Requete=wxT("SELECT date,resultat FROM data WHERE date>'")+Date+wxT("' AND id=")+NumCap+wxT(" ORDER BY date;");
    rc = sqlite3_exec(SQLite, Requete.mb_str(wxConvUTF8), callback, 0, &zErrMsg);
    if( rc!=SQLITE_OK )
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }
    if(trame.Right(1)==wxT(";"))
    {
        trame.RemoveLast();
        trame=trame+wxT("!");
    }
    else
    {
        trame=trame+wxT("!");
    }
    return trame;
    }
    else
    {
        return wxT("erreur");
    }
}

//Retourne toutes les données d'un capteur
wxString GererBDDStation::LitBDDSynchro(wxString Capteur)
{
    if(Etat==1)
    {
    int Res;
    int    NbrColone=0;
    int    NbrLigne=0;
    char **Resultat;
    int rc;
    char * zErrMsg;
    wxString chaine,NumCap,Requete,Requete1;
    wxString ex=wxT(";");

    trame.Clear();
    //On récupere le numéro du capteur
    Requete1=wxT("SELECT id FROM capteurs WHERE capteur='")+Capteur+wxT("' ;");
    if(sqlite3_get_table(SQLite, Requete1.mb_str(wxConvUTF8), &Resultat,&NbrLigne,&NbrColone, &zErrMsg) == SQLITE_OK)
    {
        if(NbrLigne==0)
       {
           FrameStation->m_textCtrlDebugServeur->AppendText(wxT("erreur"));
       }
       else
       {
           Res = atoi(Resultat[1]);
           NumCap<<Res;
       }
    }
    else
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }

    //Début de trame
    trame<<NumCap;
    trame=trame+wxT("01+");
    //Mise en forme de la requete, récupère toutes les données pour le capteur NumCap
    Requete=wxT("SELECT date,resultat FROM data WHERE id=")+NumCap+wxT(" ORDER BY date;");
    rc = sqlite3_exec(SQLite, Requete.mb_str(wxConvUTF8), callback, 0, &zErrMsg);
    if( rc!=SQLITE_OK )
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }
    if(trame.Last()==ex)
    {
        trame.RemoveLast();
        trame+=wxT("!");
    }
    else
    {
        trame+=wxT("!");
    }

    return trame;
    }
    else
    {
        return wxT("erreur");
    }
}

void GererBDDStation::EcritValeur(wxString Capteur,float Valeur)
{
    if(Etat==1)
    {


    int    NbrColone=0;
    int    NbrLigne=1;
    int Res;
    char **Resultat;
    int rc;
    char * zErrMsg;
    wxString chaine,NumCap,Requete,message,mess,DateFormate,StringValeur,DateCorrecte;
    wxDateTime Date;
    wxString Requete1;

    //Mise en forme de la date
    Date= wxDateTime::GetTimeNow();
    message=Date.FormatDate();
    mess=Date.FormatTime();
    DateFormate=message+wxT(" ")+mess;
    //Vérification que la date est bien au format AAAA-MM-JJ hh:mm:ss
    DateCorrecte=VerifieDate(DateFormate);
    StringValeur << Valeur;
    StringValeur.Replace(wxT(",") , wxT("."));

    Requete1=wxT("SELECT id FROM capteurs WHERE capteur='")+Capteur+wxT("';");

    if(sqlite3_get_table(SQLite, Requete1.mb_str(wxConvUTF8), &Resultat,&NbrLigne,&NbrColone, &zErrMsg) == SQLITE_OK)
    {
        if(NbrLigne==0)
       {
           FrameStation->m_textCtrlDebugServeur->AppendText(wxT("erreur"));
       }
       else
       {
           Res = atoi(Resultat[1]);
           NumCap<<Res;
       }
    }
    else
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
    }
    Requete= wxT("insert into data (id,date,resultat) values (")+NumCap+wxT(",DATETIME('NOW', 'localtime'),")+StringValeur +wxT(");");
    //Requete=wxT("INSERT INTO data (date,id,resultat) VALUES ('")+DateCorrecte+wxT(" ',")+NumCap+wxT(",")+StringValeur+wxT(");");
    rc =sqlite3_exec(SQLite, Requete.mb_str(wxConvUTF8) , NULL, 0, &zErrMsg) ;
    if( rc!=SQLITE_OK )
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }
    else
    {
        FrameStation->m_textCtrlDebugServeur->AppendText(wxT("Ecriture de la donnée effectué \n"));
    }
    }
    else
    {
        ;
    }
}

void GererBDDStation::SupprimeValeur()
{
    if(Etat==1)
    {

    int rc;
    wxDateTime Date;
    wxDateSpan DateS;
    char * zErrMsg;
    wxString espace=wxT(" ");
    wxString chaine,Requete,message,mess,DateFormate,DateCorrecte;

    //Obtention d'une date qui correspond à maintenant moins une semaine
    DateS.SetDays(7);
    Date= wxDateTime::GetTimeNow();
    Date.Subtract(DateS);
    message=Date.FormatDate();
    mess=Date.FormatTime();
    DateFormate=message+espace+mess;
    DateCorrecte=VerifieDate(DateFormate);

    Requete=wxT("DELETE FROM data WHERE date<'")+DateCorrecte+wxT("' ;");
    rc =sqlite3_exec(SQLite, Requete.mb_str(wxConvUTF8) , NULL, 0, &zErrMsg) ;
    if( rc!=SQLITE_OK )
    {
        chaine.Printf( wxT("SQL error: %s\n"), zErrMsg);
        FrameStation->m_textCtrlDebugServeur->AppendText(chaine);
        sqlite3_free(zErrMsg);
    }
    FrameStation->m_textCtrlDebugServeur->AppendText(wxT("Suppression de toutes les données plus vielle de 7 jours \n"));
    }
    else
    {
        ;
    }
}

//Vérification que la date est bien au format AAAA-MM-JJ hh:mm:ss
wxString GererBDDStation::VerifieDate(wxString Date)
{
    wxString Annee,Jour,Mois,Time,DateFormat;
    if( Date.GetChar(2)==wxT('/') && Date.GetChar(5)==wxT('/') && Date.GetChar(10)==wxT(' ') && Date.GetChar(13)==wxT(':') && Date.GetChar(16)==wxT(':') )
    {
        Time=Date.SubString(11,18);
        Annee=Date.SubString(6,9);
        Jour=Date.SubString(0,1);
        Mois=Date.SubString(3,4);
        DateFormat=Annee+wxT("-")+Mois+wxT("-")+Jour+wxT(" ")+Time;
        return DateFormat;
    }
    else if( Date.GetChar(4)==wxT(';') && Date.GetChar(7)==wxT(';') && Date.GetChar(10)==wxT(' ') && Date.GetChar(13)==wxT(':') && Date.GetChar(16)==wxT(':') )
    {
        return Date;
    }
    else
    {
        return wxT("erreur");
    }
    }



