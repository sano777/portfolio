#ifndef SERVEUR_H
#define SERVEUR_H

#include <string>
#include <wx/socket.h>
#include <wx/log.h>
#include "ihm/EvtStationMeteo.h"
#include <map>

#include "ServeurDonneesMeteo.h"

using namespace std;

enum
{
  // id pour les sockets
  SERVER_ID = 201,
  SOCKET_ID
};

class GererBDDStation;

class ServeurDonneesMeteo:public wxEvtHandler
{
    public:
        ServeurDonneesMeteo(EvtStationMeteo *frame, GererBDDStation *ptrGererBDD, long port, int maxclients=0);
        // maxclients=0 --> pas de limite de connexion
        ~ServeurDonneesMeteo();
        bool IsOK(){return m_serveur_running;};
        void Close();
    protected:
    private:
        EvtStationMeteo *m_frame;
        GererBDDStation *Acces_GererBDD;
        long m_port;
        int m_nombreClients;
        bool m_serveur_running;
        wxSocketServer *m_serveur;
        void OnServerEvent(wxSocketEvent& event);
        void OnSocketEvent(wxSocketEvent& event);
        void DecodeMessage (wxString message, wxSocketBase *sock);
        void EcritReponse(wxSocketBase *sock, string reponse);
        void AfficheNombreClients();
        void AfficheMessage(wxString message);
        wxString LitMessage(wxSocketBase *sock);
        wxString TrouveNomCapteur(const int);

};

#endif // SERVEUR_H
